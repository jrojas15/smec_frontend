## [4.2.1] - En desarrollo
### Fixed
- El diálogo de editar usuarios al abrirse los `mat-expansion-panel` se abrían y cerraban al abrir el diálogo.
- En algunas tablas si el valor era 0 este no mostraba nada.

### Added
- Se ha añadido `mat-progress-bar` donde ha sido necesario para mejorar la experiencia de usuario.

### Updates
- Amcharts v4.1.0.
- Mejora de la experiencia de usuario del `mat-side-nav`.

## [4.2.0] - 28/01/2019
### Fixed
- En `ChartComponent` el gráfico se realentizaba al redimensionar al no tener un ancho fijo.

### Added
- `Calibation-function-components`.
- `AppPreloadingStrategy` precarga módulos mientras se carga la aplicación.

### Updates
- Amcharts 4 v4.0.25, mejora drásticamente el rendiminedo del gráfico.
- rediseño de la interfaz de usuario.

## [4.1.2] - 07/01/2019
### Fixed
- Al añadir roles se podia añadir el mismo rol dos veces.
- Al actualizar gráfico no siempre se cierra el desplegable.
- El input de mat-select se mostraba en blanco.
- Al editar la contraseña de un usuario ahora el administrador puede resetear la contraseña.

### Added
- se añade `ngx-csv` sustituyendo a `angular5-csv` que esta obsoleto.

### Removed 
- se elimina `angular5-csv`.

### Packages Updates
- Angular v7.
- Amcharts 4 actualizado a la v4.0.16.

## [4.1.1] - 04/01/2019
### Added
- Mejoras de diseño, accesibilidad y usabilidad en `AdminModule`, `LoginModule` y `ProfileCompont`.

### Changed
- Mejoras de varios métodos en `chartService` y `chart.component`.

### Updates
- Amcharts 4 actualizado a la v4.0.13.

## [4.1.0] - 20/12/2018
### Added
- Material design lite data-table solo scss.
- chartService: en los metodos `saveSerieConfig` y `deleteSerieConfig` se muestra un snackbar si no se esta autorizado para realizar la acción.
- RoleGuardService: autoriza si se puede navegar a la ruta.
- Authorizationservice: comprueba los roles del usuario.
- AdminFeature: componentes para gestionar los usarios y sus roles.
- Ahora un usuario puede cambiar su contraseña en el perfil situado en menú de usuario.
- Se ha cambiado el calendario de Material al español.
- Un usuario podrá ver o no funcionalidades de la aplicación según el rol que tenga asignado.
- Los números decimales se han redondeado. 

### Changed
- `angular.json` ya no incorpora los scripts y css de material desing lite. Solo los necesarios para data-table.
- En `auth.interceptor.ts` el mensaje cambia si el error es `403 forbbiden`.

### Removed
- flex-box.
- signal-r service.
- Material design lite.
- `index.html` se ha eliminado la etiqueta script de signal-r.
- se ha eliminado el paquete aspnet/signal-r.
 
## [4.0.1] - 10/12/2018
### Added

- ChartComponent: `ScrollbarY` zoom vertical.
- ChartComponent: características resposive, `this.chart.responsive`.
- ChartComponent: se muestra `snackBar` al guardar o eliminar series del gráfico.
- ChartComponent: evento `dateAxis.zoom` para zoom inicial.

### Changed

- ChartComponent: se ha cambiado las dimensiones del gráfico.
- ChartComponent: el gráfico no se vuelve a cargar cuando se añade/elimina/guarda series.

### Fixed

- ChartComponent: la descripción del foco se cambiaba cuando no debia.
- ChartComponent: cuando se borraba un contaminante del gráfico(No BD), no se  actualizaba el array que almacena las series.
- ChartComponent: Al cambiar de ruta y volver si no había datos para mostrar   el contenedor del gráfico se mostraba en lugar de `DisplayMessagesComponent`.

### Removed

- `CacheRouteReuseStrategy`.

### Updates
- Versión de amcharts actualizada a la `4.0.7`.