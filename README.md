# SMECView
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build --prod --aot --build-optimizer` || `npm run build:modern`

angular-http-server -p 4200

## Code

location `C:\Devel`

## IIS

FIles location `C:\WebApp\angular`
