import { Component, OnInit } from '@angular/core';
import { Router, /*Event, NavigationStart, NavigationEnd, NavigationCancel, NavigationError, RouterOutlet*/ } from '@angular/router';
// Services
import { AuthService } from '@services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  routerEvent: Router;
  // showLoadingIndicator = true;

  constructor(
    public authService: AuthService,
    private router: Router,
  ) {

    /* this.router.events.subscribe((routerEvent: Event) => {
       if (routerEvent instanceof NavigationStart) {
         this.showLoadingIndicator = true;
       }
 
       if (routerEvent instanceof NavigationEnd || routerEvent instanceof NavigationCancel || routerEvent instanceof NavigationError) {
         this.showLoadingIndicator = false;
       }
     });*/
  }

  ngOnInit() {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['features']);
    } else {
      this.router.navigate(['login']);
    }
  }

}
