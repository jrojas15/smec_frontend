import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
const appRoutes: Routes = [

    {
        path: '',
        redirectTo: 'graf-hist',
        pathMatch: 'full',
    },
    {
        path: 'vis-rt',
        loadChildren: './displayFeature/fetch.module#FetchModule',
    },
    {
        path: 'graf-hist',
        loadChildren: './chartFeature/chart.module#ChartModule',
        data: { preload: true },
    },
    {
        path: 'graf-settings',
        loadChildren: './chartFeature/chartSettingsComponent/chart-settings.module#ChartSettingsModule',
        data: { preload: false },
    },
    {
        path: 'gest-usr',
        loadChildren: './adminFeature/admin.module#AdminModule',
    },
    {
        path: 'cal-func',
        loadChildren: './calibrationFeature/calibration-function.module#CalibrationFunctionModule',
    },
    {
        path: 'periferic',
        loadChildren: './perifericFeature/periferic-feature.module#PerifericFeatureModule',
    }
];
@NgModule({
    imports: [RouterModule.forChild(appRoutes)],
    exports: [RouterModule]
})
export class FeaturesRoutingModule { }
