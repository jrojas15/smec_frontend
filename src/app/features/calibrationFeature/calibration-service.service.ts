import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICalibrationFunction } from '@models';
import { shareReplay, refCount, publishReplay } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class CalibrationService {
  private http: HttpClient;

  constructor(@Inject(HttpClient) http: HttpClient) {
    this.http = http;
  }

  getAll() {
    return this.http.get<ICalibrationFunction[]>('api/calibrationfunction')
      .pipe(shareReplay(1), refCount());
  }

  getById(id: number) {
    return this.http.get<ICalibrationFunction>('api/calibrationfunction/' + id);
  }

  getBySensorId(id: number) {
    return this.http.get<ICalibrationFunction[]>('api/calibrationfunction/sensor/' + id);
  }

  getByAnalyzerId(id: number): Observable<ICalibrationFunction[]> {
    return this.http.get<ICalibrationFunction[]>('api/calibrationfunction/analyzer/' + id)
      .pipe(publishReplay(1), refCount());
  }

  create(item: ICalibrationFunction) {
    return this.http.post('api/calibrationfunction/create', item);
  }

  delete(id: number) {
    return this.http.delete('api/calibrationfunction/' + id);
  }

  update(item: ICalibrationFunction) {
    return this.http.patch('api/calibrationfunction/' + item.calibrationFunctionId, item);
  }

}
