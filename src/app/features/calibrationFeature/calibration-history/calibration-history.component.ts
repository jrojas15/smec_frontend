import { Component, OnInit, ViewChild, OnDestroy, ElementRef, ChangeDetectionStrategy } from '@angular/core';
import {
  MatSort,
  MatTableDataSource,
  MatPaginator,
} from '@angular/material';
import { MatDialog } from '@angular/material';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Subject, Observable } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';

import { ICalibrationFunction, ISensor } from '@models';
import { CalibrationService } from '../calibration-service.service';
import { ActivatedRoute, Router } from '@angular/router';

import { CalibrationAddComponent } from '../calibration-add/calibration-add.component';
import { AlertDialogComponent } from '@shared/modules/alertDialogModule/alert-dialog.component';

@Component({
  selector: 'app-calibration-history',
  templateUrl: './calibration-history.component.html',
  styleUrls: ['./calibration-history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalibrationHistoryComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('editSlope') slopeTemplate: ElementRef;

  private ngUnsubscribe = new Subject();

  formControl: FormGroup;

  resultsLength = 0;

  calibrationFn: Observable<ICalibrationFunction[]>;
  sensor: ISensor;
  displayedColumns = ['slope', 'intercept', 'date', 'actions'];
  secondColumns = ['prueba'];

  highlightedRows = [];
  dataSource = new MatTableDataSource([]);
  data: ICalibrationFunction[];

  measuringName: string;
  sensorId: number;
  analyzerId: string;
  description: string;

  isEnabled: boolean;
  changes = false;

  constructor(
    public dialog: MatDialog,
    private calibrationService: CalibrationService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.calibrationFn = this.route.paramMap.pipe(
      switchMap(params => {
        this.measuringName = params.get('name')!;
        this.analyzerId = params.get('analyzerId')!;
        this.description = params.get('description')!;
        this.sensorId = parseInt(params.get('sensorId')!, 10);
        return this.calibrationService.getBySensorId(this.sensorId);
      })
    );

    this.getCalibrationFunction();

    this.formControl = this.formBuilder.group({
      calibrationFunctionId: '',
      sensorId: this.sensorId,
      slope: ['', Validators.required],
      intercept: ['', Validators.required],
      date: [{ value: '', disabled: false }, Validators.required]
    });
  }

  get calibrationFunctionId() { return this.formControl.get('calibrationFunctionId'); }
  get slope() { return this.formControl.get('slope'); }
  get intercept() { return this.formControl.get('intercept'); }
  get date() { return this.formControl.get('date'); }

  disableAll(event: boolean) {
    this.isEnabled = event;
  }

  getCalibrationFunction() {
    this.changes = true;
    this.highlightedRows = [];
    this.calibrationFn.subscribe(res => {
      this.data = res;
      this.table();
      this.resultsLength = res.length;
    });
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (!this.isEnabled) {
      return true;
    }

    const dialogRef = this.dialog.open(AlertDialogComponent, {
      width: '400px',
      data: {
        message: 'Los cambios realizados no se guardarán.',
        accept: 'Aceptar'
      }
    });

    return dialogRef.afterClosed().pipe(map(result => {
      return result;
    }));
  }

  goBack() {
    this.router.navigate(['../', { id: this.analyzerId, description: this.description }], { relativeTo: this.route });
  }

  table() {
    // @ts-ignore
    this.dataSource.data = this.data;
    setTimeout(() => {
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  refresh() {
    this.getCalibrationFunction();
  }

  AddCalibrationFn() {
    const dialogRef = this.dialog.open(CalibrationAddComponent, {
      panelClass: 'add-calibration-dialog',
      width: '800px',
      data: { sensorId: this.sensorId }
    });
    dialogRef.afterClosed().
      subscribe(result => {
        if (result) {
          this.refresh();
        }
      });
  }

  selectedRow(row: ICalibrationFunction) {
    row.isEditable = true;
    this.isEnabled = true;
    this.slope!.setValue(row.slope);
    this.intercept!.setValue(row.intercept);
    this.date!.setValue(row.date);
    this.calibrationFunctionId!.setValue(row.calibrationFunctionId);
    if (this.slopeTemplate !== null) {
      setTimeout(() => {
        document.getElementById('focused-input2')!.focus();
      }, 500);
    }
  }

  updateRow() {
    this.date!.setValue(moment(this.date!.value).format('YYYY-MM-DD'));
    this.calibrationService.update(this.formControl.value)
      .subscribe(res => {
        this.refresh();
      }, (error) => {
      });
  }

  deleteRow(id: number) {
    const dialogRef = this.dialog.open(AlertDialogComponent, {
      width: '400px',
      data: {
        message: 'Se eliminará del historial.',
        accept: 'Aceptar'
      }
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          this.calibrationService.delete(id)
            .subscribe(() => {
              this.refresh();
            }, (error) => {

            });
        }
      });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
