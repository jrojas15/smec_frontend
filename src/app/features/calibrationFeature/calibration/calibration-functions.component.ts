import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';

import { Subject, Observable, NEVER, of } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';

import { ICalibrationFunction, IFocus, DisplayMessages, IError, ISelectionModel } from '@models';
import { FocusService } from '@services/api.focus.service';
import { CalibrationService } from '../calibration-service.service';
import { ErrorService } from '@services/error.service';

@Component({
  selector: 'app-calibration-functions',
  templateUrl: './calibration-functions.component.html',
  styleUrls: ['./calibration-functions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalibrationFunctionsComponent implements OnInit, OnDestroy {
  private ngUnsubscribe = new Subject();

  DisplayMessagesconf: DisplayMessages = new DisplayMessages();

  description: string;
  analyzerId: number;

  displayedColumns: string[] = ['measuring', 'slope', 'intercept', 'date'];

  dataSource$: Observable<ICalibrationFunction[]>;
  error$: Observable<IError>;
  focus$: Observable<IFocus[]>;

  constructor(
    public dialog: MatDialog,
    private _focusService: FocusService,
    private _calibrationService: CalibrationService,
    private _errorService: ErrorService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.error$ = this._errorService.errorObs$;
    this.getFocus();
    this.routerData();
  }

  getFocus() {
    this.focus$ = this._focusService.focus_GetAll()
  }

  refresh() {
    this.focus$ = this._focusService.focus_GetAll()
    setTimeout(() => {
      this.routerData();
    }, 1500);
  }

  routerData() {
    this.dataSource$ = this.route.paramMap.pipe(
      take(1),
      switchMap(params => {
        if (params.get('id')) {
          this.analyzerId = parseInt(params.get('id')!, 10);
          this.description = params.get('description')!;
          this._errorService.clearError();
          return this._calibrationService.getByAnalyzerId(this.analyzerId);
        } else {
          console.log('error when no data');
          this._errorService.setError({ code: 404, message: 'Selecciona en el menú para mostrar información' });
          return NEVER;
        }
      }));
  }

  dataFromSelection(selection: ISelectionModel) {
    this.analyzerId = selection.analyzer.analyzerId;
    this.description = selection.description;
    this.getCalibrationFns(selection.analyzer.analyzerId);
  }

  getCalibrationFns(id: number) {
    this.dataSource$ = this._calibrationService.getByAnalyzerId(id);
  }

  goToHistory(row: ICalibrationFunction) {
    this.router.navigate(
      [
        'history/',
        {
          description: this.description,
          analyzerId: this.analyzerId,
          name: row.name,
          sensorId: row.sensorId
        }
      ],
      { relativeTo: this.route });
  }

  tooltipMessage(name: string): string {
    return 'Ver historial de ' + name;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
