import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  CalibrationFunctionsComponent,
  CalibrationHistoryComponent,
  CalibrationAddComponent,
} from './index';
import { CalibrationFunctionRoutingModule } from './calibration-function-routing.module';

import { CalibrationService } from './calibration-service.service';
import { DisplayMessagesModule } from '@shared/modules/displayMessagesModule/display-messages.module';

import { MaterialModule } from '@material_modules';
import { MatSelectionModule } from '@shared/modules/matSelectionModule/mat-selection.module';
import { CalibrationHomeComponent } from './calibration-home/calibration-home.component';

@NgModule({
  imports: [
    CommonModule,
    CalibrationFunctionRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    DisplayMessagesModule,
    MaterialModule,
    MatSelectionModule,
  ],
  declarations: [
    CalibrationFunctionsComponent,
    CalibrationHistoryComponent,
    CalibrationAddComponent,
    CalibrationHomeComponent,
  ],
  providers: [
    CalibrationService
  ]
})
export class CalibrationFunctionModule { }
