import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-calibration-home',
  templateUrl: './calibration-home.component.html',
  styleUrls: ['./calibration-home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class CalibrationHomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
