import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl, NgForm, } from '@angular/forms';
import { MatTableDataSource } from '@angular/material';

import * as moment from 'moment';

import { CalibrationService } from '../calibration-service.service';


@Component({
  selector: 'app-calibration-add',
  templateUrl: './calibration-add.component.html',
  styleUrls: ['./calibration-add.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalibrationAddComponent implements OnInit {
  @Input() sensorId: number;
  @Input() isEnabled: boolean;
  @Output() update = new EventEmitter();
  @Output() isAdding = new EventEmitter();
  @ViewChild('addGroup') ngFormGroup: NgForm;

  firstFormGroup: FormGroup;
  dataSource = new MatTableDataSource([]);
  displayedColumns2 = ['input1', 'input2', 'input3', 'actions'];
  toggle$ = false;

  constructor(
    private _formBuilder: FormBuilder,
    private calibrationService: CalibrationService,
  ) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      sensorId: this.sensorId,
      slope: new FormControl('', [Validators.required]),
      intercept: new FormControl('', [Validators.required]),
      date: new FormControl('', [Validators.required]),
    });
    this.firstFormGroup.disable();
  }

  get slope() { return this.firstFormGroup.get('slope'); }
  get intercept() { return this.firstFormGroup.get('intercept'); }
  get date() { return this.firstFormGroup.get('date'); }
  get _sensorId() { return this.firstFormGroup.get('sensorId') };

  onToggle() {
    if (this.toggle$ === true) {
      this.isAdding.emit(true);
      this.firstFormGroup.enable();
      document.getElementById('focused-input')!.focus();
    } else {
      this.isAdding.emit(false);
      this.firstFormGroup.disable();
    }
  }

  resetForm() {
    this.ngFormGroup.resetForm();
    this._sensorId!.setValue(this.sensorId);
  }

  onSubmit() {
    if (this.firstFormGroup.invalid) {
      return;
    }
    const mDate = moment(this.date!.value).format('YYYY-MM-DD');
    this.date!.setValue(mDate);

    this.calibrationService.create(this.firstFormGroup.value)
      .subscribe(res => {
        this.resetForm();
        this.update.emit();
        this.isAdding.emit(false);
        this.toggle$ = false;
      }, (error) => {
      });
  }
}
