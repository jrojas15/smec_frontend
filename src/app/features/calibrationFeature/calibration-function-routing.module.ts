import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoleGuardService as RoleGuard } from '@guards/role.guard';
import { CanDeactivateGuard } from '@guards/can-deactive.guard';

import { CalibrationFunctionsComponent } from './calibration/calibration-functions.component';
import { CalibrationHistoryComponent } from './calibration-history/calibration-history.component';
import { CalibrationHomeComponent } from './calibration-home/calibration-home.component';

const appRoutes: Routes = [{
  path: '',
  component: CalibrationHomeComponent,
  canActivate: [RoleGuard],
  data: { allowedRoles: ['Administrador'] },
  children: [
    {
      path: '',
      component: CalibrationFunctionsComponent,
      data: { animation: 'heroes' }
    },
    {
      path: 'history',
      component: CalibrationHistoryComponent,
      canDeactivate: [CanDeactivateGuard],
      data: { animation: 'hero' }

    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class CalibrationFunctionRoutingModule { }
