import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserViewComponent } from './user-view/user-view.component';

import { RoleGuardService as RoleGuard } from '@guards/role.guard';

const appRoutes: Routes = [{
    path: '',
    component: UserViewComponent,
    canActivate: [RoleGuard],
    data: { allowedRoles: ['Administrador'] }
}];

@NgModule({
    imports: [RouterModule.forChild(appRoutes)],
    exports: [RouterModule]
})
export class AdminRoutingModule { }
