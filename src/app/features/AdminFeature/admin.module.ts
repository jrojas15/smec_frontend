import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DisplayMessagesModule } from '@shared/modules/displayMessagesModule/display-messages.module';

import { UserAddComponent } from './user-add/user-add.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserViewComponent } from './user-view/user-view.component';

import { AdminService } from './admin.service';
import { AdminRoutingModule } from './admin-routing.module';
import { MaterialModule } from '@material_modules';

@NgModule({
    imports: [
        CommonModule,
        AdminRoutingModule,
        DisplayMessagesModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        UserAddComponent,
        UserDetailComponent,
        UserViewComponent,
    ],
    entryComponents: [UserAddComponent],
    providers: [AdminService]
})
export class AdminModule { }
