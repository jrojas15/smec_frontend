import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Subject, BehaviorSubject } from 'rxjs';

import { AdminService } from '../admin.service';
import { UserAddComponent } from '../user-add/user-add.component';
import { User } from '../user.interface';
import { DisplayMessages } from '@models';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserViewComponent implements OnInit, OnDestroy {
  DisplayMessagesconf: DisplayMessages = new DisplayMessages();

  private ngUnsubscribe = new Subject();
  users = new Subject<User[]>();
  step = new BehaviorSubject<number>(0);
  isExpanded = new BehaviorSubject<boolean>(false);

  constructor(
    private _adminService: AdminService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit() {
    this.update();
  }

  update() {
    this._adminService.getUsers()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(res => {
        this.users.next(res);
      });
  }

  addUser() {
    this.step.next(2);
    const dialogRef = this.dialog.open(UserAddComponent, {
      panelClass: 'progress-bar-dialog',
      width: '500px',
    });
    dialogRef.afterClosed()
      .subscribe(() => {
        this.update();
        this.step.next(0);
      });
  }

  setStep(index: number) {
    this.step.next(index);
  }

  trackByUser(index: number, user: User) {
    return index;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

