import {
  Component, OnChanges, OnDestroy, Input, Output, EventEmitter, ViewChild, SimpleChanges, ChangeDetectionStrategy
} from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { User, Role } from '../user.interface';
import { AdminService } from '../admin.service';
import { AlertDialogComponent } from '@shared/modules/alertDialogModule/alert-dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class UserDetailComponent implements OnDestroy, OnChanges {
  @ViewChild('passwordForm') ngFormGroup: NgForm;

  @Input() data: User;
  @Output() update = new EventEmitter();

  private ngUnsubscribe = new Subject();

  roles: Observable<Role[]> = this._adminService.getRoles();

  form: FormGroup;
  form2: FormGroup;

  hide = true;
  _hide = true;
  __hide = true;

  constructor(
    public dialog: MatDialog,
    private _adminService: AdminService,
    private fb: FormBuilder) { }

  ngOnChanges(changes: SimpleChanges) {
    this.data = changes.data.currentValue;
    this.form = this.fb.group({
      name: [`${changes.data.currentValue.userName}`, Validators.required],
    });
    this.form2 = this.fb.group({
      newPassword: [
        '',
        [Validators.required,
        Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]],
      confirmPassword: ['', Validators.required]
    });
  }
  /**
   * -- password validator partern --
   * min 8 character length
   * lowercase
   * uppercase
   * numbers
   * special char
   */
  get name() { return this.form.get('name'); }
  get newPassword() { return this.form2.get('newPassword'); }
  get confirmPassword() { return this.form2.get('confirmPassword'); }

  equalPasswords(): boolean {
    const matched: boolean = this.newPassword!.value === this.confirmPassword!.value;
    if (matched) {
      this.form2.controls.confirmPassword.setErrors(null);
    } else {
      this.form2.controls.confirmPassword.setErrors({
        notMatched: true
      });
    }
    return matched;
  }

  onSubmitUser() {
    if (this.form.invalid) {
      return;
    }
    if (this.data.userName !== this.name!.value) {
      this._adminService.updateUserName(this.data.userName, this.name!.value)
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(() => this.data.userName = this.name!.value);
    }
  }

  onSubmitPassword() {
    if (this.form2.invalid) {
      return;
    }
    if (this.confirmPassword!.value !== null && this.newPassword!.value !== null) {
      this._adminService.ResetPassword(this.name!.value, this.confirmPassword!.value, this.newPassword!.value)
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(() => {
          this.ngFormGroup.resetForm();
        });
    }
  }

  deleteUser() {
    const dialogRef = this.dialog.open(AlertDialogComponent, {
      width: '400px',
      data: {
        message: `El usuario ${this.data.userName} será eliminado permanentemente`,
        accept: 'Aceptar'
      }
    });
    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          this._adminService.deleteUser(this.data.userName)
            .subscribe(() => {
              this.update.emit();
            });
        }
      });
  }

  addRole(role: string) {
    this._adminService.handleAddUserRole(this.data, role)
      .subscribe(() => this.update.emit());
  }

  deleteRole(role: string) {
    this._adminService.deleteUserFromRole(this.data.userName, role)
      .subscribe(() => this.update.emit());

  }

  getNewPassswordErrorMessage() {
    return this.newPassword!.hasError('required') ? 'Debes escribir una contraseña' :
      this.newPassword!.hasError('newPassword') ? 'Contraseña no válida' :
        this.newPassword!.hasError('pattern') ? 'min 8 carácteres, minúsculas, mayúsculas, números y carácteres especiales' : '';
  }

  getUserNameErrorMessage() {
    return this.name!.hasError('required') ? 'Debes escribir un nombre de usuario' : '';
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}


