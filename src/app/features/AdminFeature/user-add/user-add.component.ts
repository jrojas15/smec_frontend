import { Component, OnInit, OnDestroy, Inject, ChangeDetectionStrategy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Role } from '../user.interface';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserAddComponent implements OnInit, OnDestroy {
  private ngUnsubscribe = new Subject();
  form: FormGroup;
  message: string;
  roles: Role[];

  error = new Subject<boolean>();
  hide = true;

  constructor(
    public dialogRef: MatDialogRef<UserAddComponent>,
    private fb: FormBuilder,
    private _adminService: AdminService,
    @Inject(MAT_DIALOG_DATA) public data: any,

  ) { }

  ngOnInit() {
    this.getRole();
    this.form = this.fb.group({
      name: ['', Validators.required],
      password: [
        '',
        [Validators.required,
        Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]],
      confirmedPassword: ['', Validators.required],
      email: ['standard@localhost'],
      roleName: ['', Validators.required]
    });
  }

  get name() { return this.form.get('name'); }
  get password() { return this.form.get('password'); }
  get confirmedPassword() { return this.form.get('confirmedPassword'); }
  get email() { return this.form.get('email'); }
  get roleName() { return this.form.get('roleName'); }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }
    this._adminService.createUser(this.form.value)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(() => {
        this.error.next(false);
        this.onNoClick();
      }, (error) => {
        if (`${error.code}` === '0') {
          this.onNoClick();
        }
        if (`${error.code}` === '400') {
          this.message = error.message;
          this.error.next(true);
        }
      });
  }

  equalPasswords(): boolean {
    const matched: boolean = this.password!.value === this.confirmedPassword!.value;
    if (matched) {
      this.form.controls.confirmedPassword.setErrors(null);
    } else {
      this.form.controls.confirmedPassword.setErrors({
        notMatched: true
      });
    }
    return matched;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getRole() {
    this._adminService.getRoles()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(res => {
        this.roles = res;
      }, (error) => {
        if (`${error.code}` === '0') {
          this.onNoClick();
        }
      });
  }

  getPassswordErrorMessage() {
    return this.password!.hasError('required') ? 'Debes escribir una contraseña' :
      this.password!.hasError('newPassword') ? 'Contraseña no válida' :
        this.password!.hasError('pattern') ? 'min 8 carácteres, minúsculas, mayúsculas, números y carácteres especiales' : '';
  }

  getUserNameErrorMessage() {
    return this.name!.hasError('required') ? 'Debes escribir un nombre de usuario' : '';
  }

  trackByRole(index: number, role: Role) {
    return role.name;
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}

