import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, Role, UserCreate } from './user.interface';
import { ToastrService } from 'ngx-toastr';

import { map, mergeMap, finalize } from 'rxjs/operators';
import { of, empty } from 'rxjs';

@Injectable()
export class AdminService {
    private http: HttpClient;

    constructor(@Inject(HttpClient) http: HttpClient, private toastr: ToastrService) {
        this.http = http;
    }

    /* api/user*/
    createUser(user: UserCreate) {
        return this.http.post<UserCreate>('api/user/create', user);
    }

    getUsers() {
        return this.http.get<User[]>('api/user/list')
            .pipe(
                map(users => users.filter(user => user.userName !== 'SIGE')));
    }

    deleteUser(userName: string) {
        return this.http.delete('api/user/delete/' + userName);
    }


    handleAddUserRole(user: User, roleName: string) {
        return of(user).pipe(
            mergeMap(user$ => {
                if (user$.roleNames.length !== 2 && !user$.roleNames.includes(roleName)) {
                    return this.http.post('api/user/role/add', { name: user$.userName, role: roleName })
                        .pipe(finalize(() => this.toastr.info(`Se ha asignado el rol de ${roleName} a ${user.userName}`, '')));
                } else {
                    this.toastr.warning(`${user$.userName} ya tiene asignado el rol de ${roleName}`, '');
                    return empty();
                }
            }));
    }


    deleteUserFromRole(userName: string, roleName: string) {
        return this.http.delete('api/user/role/delete/' + userName + '/' + roleName)
            .pipe(finalize(() => this.toastr.info(`Se ha eliminado el rol de ${roleName} a ${userName}`, '')));
    }

    updateUserName(name: string, newName: string) {
        return this.http.post('api/user/update', { currentUserName: name, newUserName: newName });
    }


    ResetPassword(userName: string, currentPassword: string, newPassword: string) {
        return this.http.post('api/user/reset/password/' + userName,
            { currentPassword: currentPassword, newPassword: newPassword });
    }

    /* api/role*/
    getRoles() {
        return this.http.get<Role[]>('api/role/list');
    }


}

