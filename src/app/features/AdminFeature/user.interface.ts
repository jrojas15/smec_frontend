export interface User {
    id: number;
    userName: string;
    roleNames: string[];
}

export interface UserCreate {
    name: string;
    password: string;
    confirmedPassword: string;
    email: string;
    rolename: string;
}

export interface Role {
    name: string;
}
