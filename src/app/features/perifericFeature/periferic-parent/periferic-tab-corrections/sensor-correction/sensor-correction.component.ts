import { Component, Input, OnChanges, SimpleChanges, EventEmitter, Output, ChangeDetectionStrategy } from '@angular/core';

import { Observable } from 'rxjs';

import { ISensor, ICorrectionDTO, IError } from '@models';
import { PerifericService } from '@features/perifericFeature/periferic.service';

@Component({
  selector: 'app-sensor-correction',
  templateUrl: './sensor-correction.component.html',
  styleUrls: ['./sensor-correction.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class SensorCorrectionComponent implements OnChanges {
  @Input() sensor: ISensor;
  @Output() update = new EventEmitter();

  corrections: Observable<ICorrectionDTO[]>;

  constructor(private _perifericService: PerifericService) { }

  ngOnChanges(changes: SimpleChanges) {
    this.sensor = changes.sensor.currentValue;
    this.corrections = this._perifericService.getCorrections(this.sensor.sensorId);
  }


  deleteCorrection(id: number) {
    this._perifericService.deleteCorrection(id)
      .subscribe(() => {
        this.update.emit();
      }, (error: IError) => {
        console.log('error :', error);
      });
  }

}
