import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';

import { Subject } from 'rxjs';

import { ISensor, IAnalyzer, DisplayMessages, DefaultImages } from '@models';
import { SensorService } from '@services/api.sensor.service';

@Component({
  selector: 'app-periferic-add',
  templateUrl: './periferic-add.component.html',
  styleUrls: ['./periferic-add.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PerifericAddComponent implements OnInit, OnDestroy, OnChanges {
  @Input() analyzer: IAnalyzer;
  
  private ngUnsubscribe = new Subject();
  sensors = new Subject<ISensor[]>();

  DisplayMessagesconf: DisplayMessages = new DisplayMessages();

  constructor(private _sensorService: SensorService) { }

  ngOnInit() {
    this.DisplayMessagesconf.title = 'No hay información disponible';
    this.DisplayMessagesconf.message = '';
    this.DisplayMessagesconf.image = DefaultImages.empty_data;
    this.DisplayMessagesconf.isDataEmpty = true;
  }

  ngOnChanges(changes: SimpleChanges) {
    this.analyzer = changes.analyzer.currentValue;
    this.refresh();
  }

  refresh() {
    this._sensorService.sensor_GetByAnalyzerId(this.analyzer.analyzerId)
      .subscribe(res => this.sensors.next(res!));
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
