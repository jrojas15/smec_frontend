import {
  Component, OnChanges, Input, SimpleChanges, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef
} from '@angular/core';
import { ISensor, IPeriferic, ICorrection } from '@models';
import { PerifericService } from '@features/perifericFeature/periferic.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-add-correction',
  templateUrl: './add-correction.component.html',
  styleUrls: ['./add-correction.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddCorrectionComponent implements OnChanges {

  @Input() sensor: ISensor;
  @Input() analyzerId: number;
  @Output() update = new EventEmitter();

  periferics: Observable<IPeriferic[]>;

  constructor(private _perifericService: PerifericService) { }

  ngOnChanges(changes: SimpleChanges) {
    this.sensor = changes.sensor.currentValue;
    this.analyzerId = changes.analyzerId.currentValue;
    this.periferics = this._perifericService.getPeriferics(this.analyzerId)
      .pipe(map(periferics => periferics!.filter(periferic => periferic.perifericFormula !== null)));
  }

  addCorrection(correction: ICorrection) {
    this._perifericService.addCorrection({ perifericId: correction.perifericId, sensorId: this.sensor.sensorId })
      .subscribe(() => this.update.emit());
  }

}
