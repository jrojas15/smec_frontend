import { Component, OnInit, Input, SimpleChanges, OnChanges, OnDestroy, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { MatDialog, MatMenuTrigger } from '@angular/material';

import { forkJoin, Subject, Observable, BehaviorSubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { IPeriferic, IAnalyzer, IFormula, IError, DisplayMessages, DefaultImages } from '@models';
import { AlertDialogComponent } from '@shared/modules/alertDialogModule/alert-dialog.component';
import { PerifericService } from '@features/perifericFeature/periferic.service';

@Component({
  selector: 'app-periferic-sensor',
  templateUrl: './periferic-sensor.component.html',
  styleUrls: ['./periferic-sensor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PerifericSensorComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChild(MatMenuTrigger) menuTrigger: MatMenuTrigger;
  @Input() analyzer: IAnalyzer;

  private ngUnsubscribe = new Subject();
  error = new Subject<IError>();
  dataSource = new Subject<IPeriferic[]>();

  changed = new BehaviorSubject(false);

  formulas: Observable<IFormula[]> = this._perifericService.getFormulas();

  edit = false;
  openedMenu = false;
  enableEdition: boolean;

  DisplayMessagesconf: DisplayMessages = new DisplayMessages();

  formulaSelection: IFormula;
  noperiferics: IPeriferic[];
  displayedColumns: string[] = ['periferico', 'formula', 'other'];
  highlightedRows = [];


  constructor(
    private _perifericService: PerifericService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.DisplayMessagesconf.title = 'No se hay información disponible';
    this.DisplayMessagesconf.image = DefaultImages.empty_data;
    this.DisplayMessagesconf.isDataEmpty = true;
  }

  ngOnChanges(changes: SimpleChanges) {
    this.analyzer = changes.analyzer.currentValue;
    this.updateData();
  }

  // FIXME forjoin deprecated, change.
  updateData() {
    const noperiferics = this._perifericService.getNoPeriferics(this.analyzer.analyzerId);
    const periferics = this._perifericService.getPeriferics(this.analyzer.analyzerId);
    forkJoin(noperiferics, periferics)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(res => {
        this.noperiferics = res[0];
        this.dataSource.next(res[1]);
      });
  }

  formulaSelected(periferic: IPeriferic) {
    if (!periferic.perifericFormula) {
      this._perifericService.savePerifericFormula({ perifericId: periferic.perifericId, formulaId: this.formulaSelection.formulaId })
        .subscribe(() => {
          this.updateData();
          this.changed.next(false);
        });
    } else {
      this._perifericService.updatePerifericformula({ perifericId: periferic.perifericId, formulaId: this.formulaSelection.formulaId })
        .subscribe(() => {
          this.updateData();
          this.changed.next(false);

        });
    }
  }

  openMenu() {
    this.openedMenu = true;
  }

  closeMenu() {
    this.openedMenu = false;
  }

  createPeriferic(sensorId: number) {
    // @ts-ignore
    this._perifericService.createPeriferic({ sensorId: sensorId })
      .subscribe(() => {
        this.updateData();
      });
  }

  deletePeriferic(item: IPeriferic) {
    const dialogRef = this.dialog.open(AlertDialogComponent, {
      width: '400px',
      data: {
        message: item.name + ' dejará de ser un periférico.',
        accept: 'Aceptar'
      }
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          this._perifericService.deletePeriferic(item.perifericId)
            .subscribe(() => {
              this.updateData();
            });
        }
      });
  }

  selectedRow(row: IPeriferic) {
    row.isEditable = true;
    this.enableEdition = true;
    this.changed.next(true);
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  /** END */
}


