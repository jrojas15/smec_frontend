import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { MatDialog } from '@angular/material';

import { Subject, Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { IFocus, IAnalyzer, IError, DisplayMessages, ISelectionModel } from '@models';
import { FocusService } from '@services/api.focus.service';
import { AlertDialogComponent } from '@shared/modules/alertDialogModule/alert-dialog.component';
import { PerifericSensorComponent } from '../periferic-tab-list/periferic-sensor/periferic-sensor.component';
import { ErrorService } from '@services/error.service';

@Component({
  selector: 'app-periferic-parent',
  templateUrl: './periferic-parent.component.html',
  styleUrls: ['./periferic-parent.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PerifericParentComponent implements OnInit, OnDestroy {
  @ViewChild(PerifericSensorComponent) perifericSensor: PerifericSensorComponent;

  private ngUnsubscribe = new Subject();
  description = new Subject<string>();
  analyzer = new Subject<IAnalyzer>();

  focusList: Observable<IFocus[]> = this._focusService.focus_GetAll().pipe(take(1));

  error$: Observable<IError>;


  constructor(
    private _focusService: FocusService,
    public dialog: MatDialog,
    private _errorService: ErrorService
  ) { }

  ngOnInit() {
    this.error$ = this._errorService.errorObs$;
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.perifericSensor === undefined) {
      return true;
    }

    if (this.perifericSensor !== undefined && !this.perifericSensor.changed.getValue()) {
      return true;
    }

    const dialogRef = this.dialog.open(AlertDialogComponent, {
      width: '400px',
      data: {
        message: 'Los cambios realizados no se guardarán.',
        accept: 'Aceptar'
      }
    });

    return dialogRef.afterClosed().pipe(map(result => {
      return result;
    }));

  }

  update() {
    this.focusList = this._focusService.focus_GetAll().pipe(take(1));
  }

  dataFromSelection(selection: ISelectionModel) {
    this.description.next(selection.description);
    this.analyzer.next(selection.analyzer);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }


}
