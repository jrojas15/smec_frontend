import { Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { IPeriferic, IFormula, ICorrection, IPerifericFormula, ICorrectionDTO } from '@models';
import { HttpClient } from '@angular/common/http';

export class PerifericService {
  private http: HttpClient;

  constructor(@Inject(HttpClient) http: HttpClient
  ) {
    this.http = http;
  }

  /* Periferic api */
  getAllPeriferics(): Observable<IPeriferic[]> {
    return this.http.get<IPeriferic[]>('api/periferic');
  }

  getPerifericBySensorId(id: number): Observable<IPeriferic> {
    return this.http.get<IPeriferic>('api/periferic/' + id + '/sensor');
  }

  getPerifericById(id: number): Observable<IPeriferic> {
    return this.http.get<IPeriferic>('api/periferic/' + id);
  }

  createPeriferic(item: IPeriferic) {
    return this.http.post('api/periferic/create', item);
  }

  deletePeriferic(id: number) {
    return this.http.delete('api/periferic/delete/' + id);
  }

  getNoPeriferics(id: number): Observable<IPeriferic[]> {
    return this.http.get<IPeriferic[]>('api/periferic/' + id + '/noperiferics');
  }

  getPeriferics(id: number): Observable<IPeriferic[]> {
    return this.http.get<IPeriferic[]>('api/periferic/' + id + '/periferics');
  }
  /* FormulaPeriferic */
  getPerifericFormula(id: number): Observable<IPerifericFormula> {
    return this.http.get<IPerifericFormula>('api/periferic/' + id + '/formula');
  }


  getPerifericFormulas(): Observable<IPerifericFormula[]> {
    return this.http.get<IPerifericFormula[]>('api/periferic/formula');
  }

  /** Formula api */
  getFormulas(): Observable<IFormula[]> {
    return this.http.get<IFormula[]>('api/formula');
  }

  savePerifericFormula(item: IPerifericFormula) {
    return this.http.post('api/formula/create', item);
  }

  updatePerifericformula(item: IPerifericFormula) {
    return this.http.patch('api/formula/update/' + item.perifericId, item);
  }

  /** corrections api */
  getCorrections(id: number): Observable<ICorrectionDTO[]> {
    return this.http.get<ICorrectionDTO[]>('api/corrections/' + id + '/sensor');
  }

  addCorrection(item: ICorrection) {
    return this.http.post('api/corrections/create', item);
  }

  deleteCorrection(id: number) {
    return this.http.delete('api/corrections/delete/' + id);
  }

}
