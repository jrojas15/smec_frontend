import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PerifericParentComponent } from './periferic-parent/periferic-root/periferic-parent.component';

import { RoleGuardService as RoleGuard } from '@guards/role.guard';
import { CanDeactivateGuard } from '@guards/can-deactive.guard';
const routes: Routes = [{
  path: '',
  component: PerifericParentComponent,
  canActivate: [RoleGuard],
  canDeactivate: [CanDeactivateGuard],
  data: { allowedRoles: ['Administrador', 'Usuario'] }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerifericFeatureRoutingModule { }
