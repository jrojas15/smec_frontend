import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { PerifericFeatureRoutingModule } from './periferic-feature-routing.module';
import { MaterialModule } from '@material_modules';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DisplayMessagesModule } from '@shared/modules/displayMessagesModule/display-messages.module';
import { MatSelectionModule } from '@shared/modules/matSelectionModule/mat-selection.module';
import { PerifericService } from './periferic.service';

import { PerifericParentComponent } from './periferic-parent/periferic-root/periferic-parent.component';
import { PerifericSensorComponent } from './periferic-parent/periferic-tab-list/periferic-sensor/periferic-sensor.component';
import { PerifericAddComponent } from './periferic-parent/periferic-tab-corrections/corrections-parent/periferic-add.component';
import { SensorCorrectionComponent } from './periferic-parent/periferic-tab-corrections/sensor-correction/sensor-correction.component';
import { AddCorrectionComponent } from './periferic-parent/periferic-tab-corrections/add-correction/add-correction.component';


@NgModule({
  entryComponents: [PerifericParentComponent],
  declarations: [
    PerifericParentComponent,
    PerifericSensorComponent,
    PerifericAddComponent,
    SensorCorrectionComponent,
    AddCorrectionComponent,
  ],
  imports: [
    CommonModule,
    PerifericFeatureRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    DisplayMessagesModule,
    MatSelectionModule,
  ],
  exports: [
    MatCardModule
  ],
  providers: [PerifericService]
})
export class PerifericFeatureModule { }
