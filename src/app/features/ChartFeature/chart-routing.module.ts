import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChartComponent } from './chartComponent/chart.component';

import { RoleGuardService as RoleGuard } from '@guards/role.guard';

const appRoutes: Routes = [
  {
    path: '',
    component: ChartComponent,
    canActivate: [RoleGuard],
    data: { allowedRoles: ['Administrador', 'Usuario'] }
  },
];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class ChartRoutingModule { }
