import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@material_modules';
import { ChartSettingsComponent } from './chart-settings.component';
import { ChartService } from '../chart.service';
import { ChartSettingsRoutingModule } from './chart-settings-routing.module';
import { AddChartSerieComponent } from '../childComponents/add-chart-serie/add-chart-serie.component';
import { ChartSettingsService } from './chart-settings.service';
import { DisplayMessagesModule } from '@shared/modules/displayMessagesModule/display-messages.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ChartSettingsRoutingModule,
        MaterialModule,
        DisplayMessagesModule
    ],
    declarations: [
        ChartSettingsComponent,
        AddChartSerieComponent
    ],
    entryComponents: [ChartSettingsComponent],
    providers: [ChartService, ChartSettingsService]
})
export class ChartSettingsModule { }
