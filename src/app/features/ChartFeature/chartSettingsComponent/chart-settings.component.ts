/* tslint:disable */
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';

import { ChartSettingsService } from './chart-settings.service';
import { DisplayMessages, DefaultImages, IError } from '@models';
import { take } from 'rxjs/operators';

interface Chart {
  id: number;
  name: string;
  children?: Chart[];
}

/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
  id: number;
}


@Component({
  selector: 'app-chart-settings',
  templateUrl: './chart-settings.component.html',
  styleUrls: ['./chart-settings.component.scss']
})
export class ChartSettingsComponent implements OnInit, OnDestroy {
  DisplayMessagesconf: DisplayMessages = new DisplayMessages();

  chart_Data: Chart[];
  key = 'chart';
  storageItem: JSON;
  storageSupport = window.localStorage;

  hide = true;

  error: IError;

  private transformer = (node: Chart, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
      id: node.id
    };
  }

  getLevel = (node: ExampleFlatNode) => node.level;

  treeControl = new FlatTreeControl<ExampleFlatNode>(
    node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
    this.transformer, this.getLevel, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor(
    private _settingsService: ChartSettingsService
  ) {
  }

  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

  ngOnInit() {
    this.storageItem = JSON.parse(localStorage.getItem(this.key)!)
    this.getChartSeries();
    setTimeout(() => {
      this.hide = false;
    }, 2000);

  }

  getChartSeries() {
    this._settingsService.getChartSeries()
      .pipe(take(1))
      .subscribe(charts => {
        let parent: Chart;
        let child: Chart;

        this.chart_Data = charts.map(chart => {
          return parent = {
            id: chart.chartId,
            name: chart.description,
            children: chart.series!.map(serie => {
              return child = {
                id: serie.serieId!,
                name: serie.name
              }
            })
          }
        })

        if (this.chart_Data.length > 0) {
          this.dataSource.data = this.chart_Data;
        } else {
          this.DisplayMessagesconf.title = 'No se ha encontrado ningún gráfico';
          this.DisplayMessagesconf.message = 'Ir a "Añadir gráfico"';
          this.DisplayMessagesconf.image = DefaultImages.empty_data;
          this.DisplayMessagesconf.isDataEmpty = true;
        }
      }, (error: IError) => {
        this.error = error;
      });
  }

  deleteChart(chart: Chart) {
    this._settingsService.deleteChart(chart.id)
      .subscribe(() => {
        if (this.storageItem) {
          localStorage.removeItem(this.key);
        }
        this.getChartSeries();
      });
  }

  toggleDefault(chart: Chart) {
    if (this.storageSupport) {
      let obj = { name: chart.name, chartId: chart.id };
      localStorage.setItem(this.key, JSON.stringify(obj))
      this.storageItem = JSON.parse(localStorage.getItem(this.key)!)
      this.getChartSeries();
    }
  }

  deleteDefault() {
    localStorage.removeItem(this.key)
    this.storageItem = JSON.parse(localStorage.getItem(this.key)!);
    this.getChartSeries();
  }

  ngOnDestroy() {
    this.hide = true;
  }
}




