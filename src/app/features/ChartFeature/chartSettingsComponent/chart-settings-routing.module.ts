import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoleGuardService as RoleGuard } from '@guards/role.guard';

import { ChartSettingsComponent } from './chart-settings.component';

const appRoutes: Routes = [
    {
        path: '',
        component: ChartSettingsComponent,
        canActivate: [RoleGuard],
        data: { allowedRoles: ['Administrador'] }
    },
];

@NgModule({
    imports: [RouterModule.forChild(appRoutes)],
    exports: [RouterModule]
})
export class ChartSettingsRoutingModule { }
