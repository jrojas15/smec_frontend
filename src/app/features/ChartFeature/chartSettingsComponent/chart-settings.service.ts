import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IChart, ISerie } from '@models';

@Injectable()
export class ChartSettingsService {
  private http: HttpClient;

  constructor(@Inject(HttpClient) http: HttpClient) {
    this.http = http;
  }


  getChartSeries(): Observable<IChart[]> {
    return this.http.get<IChart[]>('api/chart');
  }


  addChart(chart: IChart) {
    return this.http.post('api/chart/create', chart);

  }

  deleteChart(id: number) {
    return this.http.delete('api/chart/' + id);
  }

  addSeries(series: ISerie[]) {
    return this.http.post('api/serie', series);

  }


  getSeries(): Observable<ISerie[] | null> {
    return this.http.get<ISerie[]>('api/serie');
  }
}
