import {
  Component,
  OnInit,
  OnDestroy,
  NgZone,
  ChangeDetectionStrategy
} from "@angular/core";
import { switchMap, shareReplay } from "rxjs/operators";
import { Subject, Observable, of } from "rxjs";
// @ts-ignore
import * as am4core from "@amcharts/amcharts4/core";
// @ts-ignore
import * as am4charts from "@amcharts/amcharts4/charts";
// @ts-ignore
import am4lang_es_ES from "@amcharts/amcharts4/lang/es_ES";
// @ts-ignore
import am4themes_material from "@amcharts/amcharts4/themes/material";

import { IError, IChart, IChartForm, IChartAnalogData } from "@models";
import { ChartService } from "../chart.service";
import { ErrorService } from "@services/error.service";

@Component({
  selector: "app-chart",
  templateUrl: "./chart.component.html",
  styleUrls: ["./chart.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartComponent implements OnInit, OnDestroy {
  private ngUnsubscribe = new Subject();
  header$ = new Subject<IComponentHeader | undefined>();

  dataSource$: Observable<any>;
  error$: Observable<IError>;

  public chart: am4charts.XYChart;

  init_date$: Date;
  end_date$: Date;

  constructor(
    public chartService: ChartService,
    private zone: NgZone,
    private _errorService: ErrorService
  ) {}

  ngOnInit() {
    this.error$ = this._errorService.errorObs$;
  }

  requestChartData(event: IChartForm) {
    if (event.chartId) {
      this.init_date$ = event.initialDate;
      this.end_date$ = event.finalDate;
      this._errorService.clearError();
      this.dataSource$ = this.chartService.getChartbyId(event.chartId).pipe(
        switchMap(chart =>
          this.chartService
            .getChartWithSeries(
              chart.series!,
              event.initialDate,
              event.finalDate
            )
            .pipe(switchMap(data => this.makeChart(data, chart)))
        ),
        shareReplay()
      );
    } else {
      this._errorService.setError({
        code: 404,
        message:
          "Selecciona un gráfico para mostrar o marca uno como predeterminado en ajustes"
      });
    }
  }

  makeChart(dataProvider: IChartAnalogData[], chartData: IChart) {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
      am4core.useTheme(am4themes_material);
      am4core.options.minPolylineStep = 5;

      this.chart = am4core.create("chartdiv", am4charts.XYChart);
      this.chart.colors.step = 3;
      this.chart.invalidateData();
      this.chart.nonScalingStroke = true;
      this.chart.language.locale = am4lang_es_ES;
      this.chart.data = dataProvider;
      // this.chart.responsive.enabled = true;

      this.chart.dateFormatter.inputDateFormat = "MM/dd/yyyy HH:mm:s";
      this.chart.numberFormatter.numberFormat = "#.00";
      // this.chart.tooltip.pointerOrientation = 'horizontal';

      const dateAxis = this.chart.xAxes.push(new am4charts.DateAxis());
      dateAxis.periodChangeDateFormats.setKey("hour", "[bold]HH:mm a");
      dateAxis.renderer.grid.template.location = 0;
      dateAxis.tooltipDateFormat = "HH:mm, d MMMM";
      dateAxis.dataFields.date = "date";
      dateAxis.baseInterval = {
        timeUnit: "second",
        count: 1
      };

      const valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.title.text = chartData.description;

      const serie = chartData.series!.map((s, index) => {
        const series1 = this.chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueY = s.name.toLowerCase();
        series1.dataFields.dateX = "date";
        series1.name = s.name;

        series1.strokeWidth = 2;
        series1.tooltipText =
          "{name}: [bold]{valueY} " + s.sensor!.unit.name + "[/]";
        // series1.tensionX = 0.8;
      });

      this.chart.invalidateData();
      this.chart.legend = new am4charts.Legend();
      this.chart.cursor = new am4charts.XYCursor();

      // Create vertical scrollbar and place it before the value axis
      this.chart.scrollbarY = new am4core.Scrollbar();
      this.chart.scrollbarY.parent = this.chart.leftAxesContainer;
      this.chart.scrollbarY.toBack();

      // Add Scrollbar
      this.chart.scrollbarX = new am4core.Scrollbar();
      this.chart.scrollbarX.thumb.minWidth = 50;
      this.chart.events.on("datavalidated", function(ev) {
        dateAxis.zoom({ start: 0, end: 0.4 });
      });

      if (this.chart.isReady) {
        this.header$.next({
          description: chartData.description,
          init_date: this.init_date$,
          end_date: this.end_date$
        });
      }
    });

    return of({});
  }

  downloadChartData(event: IChartForm) {
    this.chartService.generateFile(
      event.chartId,
      event.initialDate,
      event.finalDate
    );
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
}

export interface IComponentHeader {
  description: string;
  init_date: Date;
  end_date: Date;
}
