import { Injectable, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { MatSnackBar } from "@angular/material";
import { Observable } from "rxjs";
import * as moment from "moment";

import { IChart, ISerie } from "@models";

import { CurrentAnalogDataService } from "@services/api.currentAnalogData.service";

import { ToastrService } from "ngx-toastr";
import { switchMap, tap, finalize } from "rxjs/operators";
import { saveAs } from "file-saver";

@Injectable()
export class ChartService {
  private http: HttpClient;

  constructor(
    public snackBar: MatSnackBar,
    private toastr: ToastrService,
    private _currentAnalogDataService: CurrentAnalogDataService,
    @Inject(HttpClient) http: HttpClient
  ) {
    this.http = http;
  }

  getChartSeries(): Observable<IChart[]> {
    return this.http.get<IChart[]>("api/chart");
  }

  getChartbyId(id: number): Observable<IChart> {
    return this.http.get<IChart>("api/chart/" + id);
  }

  getChartWithSeries(series: ISerie[], initialDate: Date, finalDate: Date) {
    return this._currentAnalogDataService.chart_GetHistoricalAnalogData(
      series,
      initialDate,
      finalDate
    );
  }

  notFoundMessage(initialDate: Date, finalDate: Date): string {
    const message =
      "No se han encontrado datos entre " +
      moment(initialDate).format("DD/MM/YYYY") +
      " y " +
      moment(finalDate).format("DD/MM/YYYY");
    return message;
  }

  notFoundToaster() {
    this.toastr.info("No hay datos para descargar el archivo", "");
  }

  /** CSV REPORT */
  generateFile(id: number, initialDate: Date, finalDate: Date) {
    const fileName = "report-from-chart";
    this.getChartbyId(id)
      .pipe(
        tap(() => console.log("report petition initialized ...")),
        switchMap(chart =>
          this.sendFileData(fileName, chart.series!, initialDate, finalDate)
        ),
        finalize(() => console.log("... report petition finished"))
      )
      .subscribe((data: any) => {
        this.downaloadFile(data.fileName);
      });
  }

  sendFileData(
    name: string,
    series: ISerie[],
    initialDate: Date,
    finalDate: Date
  ) {
    return this.http.post("api/report/csvreport", {
      fileName: name,
      series: series,
      start_date: initialDate,
      end_date: finalDate
    });
  }

  downaloadFile(fileName: string) {
    this.http
      .get("api/FileProvider/download/" + fileName + ".xlsx", {
        headers: {},
        responseType: "blob"
      })
      .subscribe(data => {
        const newFile = new File([data], fileName + ".xlsx", {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        });
        saveAs(newFile);
      });
  }

}
  /* downloadFile(data: [data[], IChart], initialDate: Date, finalDate: Date) {
          // TODO: refactor to JSReport api service
          this.toastr.success('Archivo descargado con éxito', '');
          if (data && data[0].length > 0) {
              const names: string[] = [];
              const _name = data[1].series.map(serie => names.push(serie.name));
              const _initDate = moment(initialDate).format('YYYY-MM-DD');
              const _finalDate = moment(finalDate).format('YYYY-MM-DD');
              const csvData = data[0];
              let filter;
              const empty = {};
              // cambiar el formato de las fechas
              for (let i = 0; i < csvData.length; i++) {
                  csvData[i].date = empty['date'] = moment(csvData[i].date).format('DD-MM-YYYY HH:mm');
              }

              const options = {
                  fieldSeparator: ',',
                  quoteStrings: '',
                  decimalseparator: '.',
                  showLabels: true,
                  showTitle: true,
                  title: data[1].description,
                  useBom: true,
                  headers: ['Timestamp', names]
              };

              // eliminar las entradas que sean null, undefined o con valor a 0
              for (let i = 0; i < names.length; i++) {
                  filter = csvData.filter(x => {
                      return x[names[i]] !== null || undefined || 0;
                  });
              }
              const csvFile = new ngxCsv(filter, data[1].description + _initDate + '_' + _finalDate, options);

              if (csvFile.data) {
                  this.toastr.success('Archivo descargado con éxito', '');
              }
          }
  }*/

