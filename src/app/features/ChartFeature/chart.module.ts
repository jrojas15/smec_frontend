import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ChartComponent } from './chartComponent/chart.component';
import { ChartRoutingModule } from './chart-routing.module';

import { ChartService } from './chart.service';

import { DisplayMessagesModule } from '@shared/modules/displayMessagesModule/display-messages.module';
import { FabButtonModule } from '@shared/modules/fabButtonModule/fabButton.module';

import { MaterialModule } from '@material_modules';
import { SelectChartComponent } from './childComponents/select-chart/select-chart.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ChartRoutingModule,
    DisplayMessagesModule,
    MaterialModule,
    FabButtonModule
  ],
  declarations: [
    ChartComponent,
    SelectChartComponent
  ],
  entryComponents: [ChartComponent],
  providers: [ChartService]
})
export class ChartModule { }
