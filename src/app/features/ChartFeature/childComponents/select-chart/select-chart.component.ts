import { Component, OnInit, Output, EventEmitter, ViewChild, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { ChartService } from '../../chart.service';
import { IChart, IError, IChartForm } from '@models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatMenuTrigger } from '@angular/material';

@Component({
  selector: 'app-select-chart',
  templateUrl: './select-chart.component.html',
  styleUrls: ['./select-chart.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectChartComponent implements OnInit {
  @ViewChild(MatMenuTrigger) menuTrigger: MatMenuTrigger;

  @Output() makeChart = new EventEmitter();
  @Output() download = new EventEmitter();

  form: FormGroup;

  charts: IChart[];
  openedMenu = false;

  _initialDate: Date = new Date();
  _finalDate: Date;
  max_date = new Date();
  date = new Date();
  tomorrow = new Date(this.date.getTime());
  // error: IError;

  constructor(private _chartService: ChartService, private fb: FormBuilder) { }

  ngOnInit() {
    const storageItem = JSON.parse(localStorage.getItem('chart')!);
    this.datesHandler();
    this.form = this.fb.group({
      chartId: [null, Validators.required],
      initialDate: [{ value: this._initialDate, disabled: false }, Validators.required],
      finalDate: [{ value: this._finalDate, disabled: false }, Validators.required]
    });
    if (storageItem) {
      this.chartId.setValue(parseInt(storageItem.chartId));
    }
    this.makeChart.emit(<IChartForm>this.form.value);
    this.getAllChartSeries();
  }

  get chartId() { return this.form.get('chartId')!; }
  get initialDate() { return this.form.get('initialDate'); }
  get finalDate() { return this.form.get('finalDate'); }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }
    this.makeChart.emit(this.form.value);
    this.menuTrigger.closeMenu();
  }

  startDownload() {
    if (this.form.invalid) {
      return;
    }
    this.download.emit(<IChartForm>this.form.value);
    this.menuTrigger.closeMenu();
  }

  getAllChartSeries() {
    this._chartService.getChartSeries()
      .subscribe(charts => {
        this.charts = charts;
        // this.error = null;
      }, (error: IError) => {
        //this.error = error;
      });
  }

  datesHandler() {
    this._initialDate = new Date(2018, 10, 26);
    this._finalDate = new Date(2018, 10, 27);
    // this._initialDate = new Date();
    // this.tomorrow.setDate(this.date.getDate() + 1);
    // this._finalDate = new Date(this.tomorrow);
  }

  openMenu() {
    this.openedMenu = true;
  }

  closeMenu() {
    this.openedMenu = false;
  }
}
