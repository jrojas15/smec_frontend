import { Component, OnInit, Output, EventEmitter, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { MatStepper, MatSelectionList, MatListOption } from '@angular/material';

import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { FocusService } from '@services/api.focus.service';
import { ChartSettingsService } from '@features/chartFeature/chartSettingsComponent/chart-settings.service';

import { IAnalyzer, IFocus, ISensor, ISerie, IError, IChart } from '@models';

@Component({
  selector: 'app-add-chart-serie',
  templateUrl: './add-chart-serie.component.html',
  styleUrls: ['./add-chart-serie.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class AddChartSerieComponent implements OnInit {
  @ViewChild('stepper') stepper: MatStepper;
  @ViewChild('paso1Form') ngFormGroup: NgForm;
  @ViewChild('paso2Form') ngFormGroup2: NgForm;
  @ViewChild('list') checkBoxList: MatSelectionList;
  @Output() update = new EventEmitter();

  isLinear = false;

  focus: IFocus;
  chart: IChart;
  // TODO: cambiar a observables
  analyzers: IAnalyzer[];
  sensors: ISensor[];
  series: ISerie[];

  description: string;

  paso1: FormGroup;
  paso2: FormGroup;
  paso3: FormGroup;

  focusList: Observable<IFocus[]> = this._focusService.focus_GetAll();


  constructor(
    private _focusService: FocusService,
    private _settingsService: ChartSettingsService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.paso1 = this.fb.group({
      _focus: ['', Validators.required]
    });
    this.paso2 = this.fb.group({
      _analyzer: ['', Validators.required]
    });
  }

  get _focus() { return this.paso1.get('_focus'); }
  get _analyzer() { return this.paso2.get('_analyzer'); }

  resetforms() {
    Object.keys(this.paso1.controls).forEach(key => {
      this.paso1.controls[key].setErrors(null);
    });
  }

  focusSelection(item: IFocus) {
    this.focus = item;
    this.analyzers = item.analyzers;
  }

  analyzerSelection(item: IAnalyzer) {
    this.sensors = item.sensors;
    this.description = this.focus.description + ' - ' + item.model;
  }

  seriesSelection(optionsSelected: MatListOption[]) {
    this.checkBoxList.selectedOptions.select;
    this.series = [];
    for (const option of optionsSelected) {
      const _serie: ISerie = {
        sensorId: option.value.sensorId,
        chartId: this.focus.focusId,
        name: option.value.measuringComponent.name
      };
      this.series.push(_serie);
    }
  }

  saveChart() {
    this.chart = { chartId: this.focus.focusId, description: this.description };
    const createChart = this._settingsService.addChart(this.chart);
    const createSeries = this._settingsService.addSeries(this.series);
    createChart.pipe(
      mergeMap(() => createSeries))
      .subscribe(val => {
        this.update.emit();
        this.stepper.reset();
        this.ngFormGroup.resetForm();
        this.checkBoxList.deselectAll();
        this.series = [];
      }, (error: IError) => {
      });
  }

  resetStepper() {
    this.stepper.reset();
    this.checkBoxList.deselectAll();
    this.series = [];
    this.ngFormGroup.resetForm();
    this.ngFormGroup2.resetForm();
  }
}
