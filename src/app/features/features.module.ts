import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeaturesRoutingModule } from './features-routing.module';


import { AlertDialogModule } from '@shared/modules/alertDialogModule/alert-dialog.module';

@NgModule({
    imports: [
        CommonModule,
        FeaturesRoutingModule,
        AlertDialogModule
    ],
    exports: [],
    declarations: [],
    providers: []
})
export class FeaturesModule { }
