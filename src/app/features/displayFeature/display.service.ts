import { Injectable } from '@angular/core';
import { FocusService } from '@services/api.focus.service';
import { CurrentAnalogDataService } from '@services/api.currentAnalogData.service';

import { IFocus, ISensor } from '@models';

import { map, switchMap, bufferCount, skipWhile, tap, concatMap } from 'rxjs/operators';
import { from, zip, timer } from 'rxjs';
import { ErrorService } from '@services/error.service';

const REFRESH_INTERVAL = 10000;

@Injectable()
export class DisplayService {

  public focusObs$ = this._focusService.focus_GetAll()
    .pipe(
      skipWhile(focus => focus.length === 0),
      map(focus => focus.find(f => f.inService === 1))
    );

  constructor(
    private _focusService: FocusService,
    private _currentSensorDataService: CurrentAnalogDataService,
    private _errorService: ErrorService
  ) { }

  requestFocus() {
    const timer$ = timer(0, REFRESH_INTERVAL);
    return timer$.pipe(
      switchMap(() => this.focusObs$));
  }

  requestCurrentData(id: number) {
    return this._currentSensorDataService.sensor_GetCurrentAnalogData(id);
  }

  handleRequestedData(data: IFocus) {
    const _sensor: ISensor[] = [];
    data.analyzers.map(analyzer => {
      analyzer.sensors.map(sensor => {
        _sensor.push(sensor);
      });
    });
    const source = from(_sensor);
    const result = source.pipe(
      concatMap(({ sensorId }) => this.requestCurrentData(sensorId)),
      bufferCount(_sensor.length),
    );
    return result;
  }

  zipDataObservable() {
    return zip(
      this.requestFocus(),
      this.requestFocus().pipe(switchMap((focus: IFocus) => this.handleRequestedData(focus)))
    ).pipe(
      tap((res: [IFocus, any[]]) => {
        if (res[0] && res[1].length > 0) {
          this._errorService.clearError();
        }
      })
    );
  }
}

