import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FetchRoutingModule } from './fetch-routing.module';
import { FetchDataComponent } from './fetchData/fetch-data.component';


import { DisplayMessagesModule } from '@shared/modules/displayMessagesModule/display-messages.module';

import { DisplayService } from './display.service';
import { MaterialModule } from '@material_modules';

@NgModule({
  imports: [
    CommonModule,
    FetchRoutingModule,
    DisplayMessagesModule,
    MaterialModule
  ],
  declarations: [
    FetchDataComponent
  ],
  providers: [
    DisplayService
  ]
})
export class FetchModule { }
