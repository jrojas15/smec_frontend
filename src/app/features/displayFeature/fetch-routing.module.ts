import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FetchDataComponent } from './fetchData/fetch-data.component';

import { RoleGuardService as RoleGuard } from '@guards/role.guard';
const appRoutes: Routes = [{
  path: '',
  component: FetchDataComponent,
  canActivate: [RoleGuard],
  data: { allowedRoles: ['Administrador', 'Usuario'] }
}];
@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class FetchRoutingModule { }
