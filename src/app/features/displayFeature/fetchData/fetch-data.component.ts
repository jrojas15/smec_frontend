import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { DisplayMessages, IFocus } from '@models';
import { DisplayService } from '../display.service';
import { ErrorService } from '@services/error.service';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html',
  styleUrls: ['./fetch-data.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FetchDataComponent implements OnInit {
  DisplayMessagesconf: DisplayMessages = new DisplayMessages();
  displayedColumns = ['contaminante', 'value', 'calibrado', 'corregido', 'validado', 'vle', 'unidad', 'estado'];

  public dataSource: Observable<[IFocus, any[]]>;
  public error = this._errorService.errorObs$;

  constructor(
    private _displayService: DisplayService,
    private _errorService: ErrorService
  ) { }

  ngOnInit() {
    this.getData()
  }

  getData() {
    this.dataSource = this._displayService.zipDataObservable();
  }
}
