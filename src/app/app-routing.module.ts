import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppPreloadingStrategy } from './preload-strategy';

import { AuthGuard } from '@guards/auth.guard';
import { RoleGuardService as RoleGuard } from '@guards/role.guard';

import { LoginViewComponent } from '@shared/modules/LoginModule/login-view.component';
import { FeatureRootComponentComponent } from '@shared/components/dashboradComponent/feature-root-component.component';
import { ConnectionErrorComponent } from '@shared/components/connectionErrorComponent/connection-error.component';
import { NotFoundComponent } from '@shared/components/not-found/not-found.component';


const appRoutes: Routes = [
  {
    path: 'login', component: LoginViewComponent,
  },
  {
    path: 'error', component: ConnectionErrorComponent,
  },
  { path: '404', component: NotFoundComponent },
  {
    path: 'features',
    component: FeatureRootComponentComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        loadChildren: './features/features.module#FeaturesModule',
        canActivate: [AuthGuard, RoleGuard]
      }
    ]
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '404',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { preloadingStrategy: AppPreloadingStrategy })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
