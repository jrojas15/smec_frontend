import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, NavigationEnd } from '@angular/router';
// Services
import { AuthService } from '@services/auth.service';
import { IError } from '@models';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginViewComponent implements OnInit {
  form: FormGroup;
  isLogged = new Subject<boolean>();
  hide = true;
  showLoadingIndicator = new Subject<boolean>();
  isConnected = new Subject<boolean>();
  noConnMessage: string;
  errorMessage: string;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authservice: AuthService,
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      Username: ['', Validators.required],
      Password: ['', Validators.required]
    });
  }

  get Username() { return this.form.get('Username'); }
  get Password() { return this.form.get('Password'); }

  onSubmit() {
    this.isConnected.next(true);
    this.showLoadingIndicator.next(true);
    if (this.form.invalid) {
      this.showLoadingIndicator.next(false);
      return;
    }
    const username = this.form.value.Username;
    const password = this.form.value.Password;
    this.authservice.login(username, password)
      .subscribe(() => {
        setTimeout(() => {
          this.router.navigate(['features']);
        }, 2000);
      }, (error: IError) => {
        if (error.code === 0) {
          this.showLoadingIndicator.next(false);
          this.isLogged.next(false);
          this.isConnected.next(false);
        }

        if (error.code === 401) {
          this.showLoadingIndicator.next(false);
          this.isLogged.next(false);
          this.form.setErrors({
            'auth': 'Usuario o contraseña inconrrectos'
          });
        }
      });
  }

  onBack() {
    this.router.navigate(['features']);
  }
  // retrieve a FormControl
  getFormControl(name: string) {
    return this.form.get(name);
  }
  // returns TRUE if the FormControl is valid
  isValid(name: string) {
    const e = this.getFormControl(name);
    return e && e.valid;
  }
  // returns TRUE if the FormControl has been changed
  isChanged(name: string) {
    const e = this.getFormControl(name);
    return e && (e.dirty || e.touched);
  }
  // returns TRUE if the FormControl is invalid after user changes
  hasError(name: string) {
    const e = this.getFormControl(name);
    return e && (e.dirty || e.touched) && !e.valid;
  }

  getPassswordErrorMessage() {
    return this.Password!.hasError('required') ? 'Debes escribir una contraseña' :
      this.Password!.hasError('Password') ? 'Contraseña no válida' : '';
  }

  getUserNameErrorMessage() {
    return this.Username!.hasError('required') ? 'Debes escribir un nombre de usuario' : '';
  }
}
