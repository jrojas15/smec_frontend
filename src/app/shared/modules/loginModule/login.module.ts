import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoginViewComponent } from './login-view.component';
import { AuthService } from '@services/auth.service';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { MaterialModule } from '@material_modules';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatProgressSpinnerModule,
        MaterialModule
    ],
    declarations: [
        LoginViewComponent
    ],
    providers: [
        AuthService
    ]
})
export class LoginModule { }
