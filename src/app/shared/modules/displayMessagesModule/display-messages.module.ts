import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisplayMessagesComponent } from './display-messages.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  imports: [
    CommonModule,
    MatProgressBarModule,
    MatIconModule,
    MatButtonModule

  ],
  declarations: [
    DisplayMessagesComponent
  ],
  exports: [
    CommonModule,
    DisplayMessagesComponent,
    MatProgressBarModule,
  ]
})
export class DisplayMessagesModule { }
