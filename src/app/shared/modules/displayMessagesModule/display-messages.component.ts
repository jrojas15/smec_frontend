import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges, ChangeDetectionStrategy } from '@angular/core';
import { IError, DefaultImages } from '@models';
import { LoadingService } from '@services/loading.service';

import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-display-messages',
  templateUrl: './display-messages.component.html',
  styleUrls: ['./display-messages.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class DisplayMessagesComponent implements OnInit, OnChanges {
  @Input() error: IError;
  @Input() button = true;
  @Output() update = new EventEmitter();

  default = DefaultImages;

  loadingSubscription = this.loadingScreenService.loadingStatus.pipe(debounceTime(500));

  show = new Subject<boolean>();

  constructor(private loadingScreenService: LoadingService) { }

  ngOnInit() {
    this.show.next(false);
    setTimeout(() => {
      this.show.next(true);
    }, 1000);
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('mesasges changes => ', changes);
    if (changes['error']) {
      this.error = changes.error.currentValue;
    }
    if (changes['button']) {
      this.button = changes.button.currentValue;
    }
  }

  sendUpdate() {
    this.update.emit();
  }
}


