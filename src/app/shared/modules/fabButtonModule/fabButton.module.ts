import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpeedDialFabComponent } from './speed-dial-fab.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
    imports: [
        CommonModule,
        MatTooltipModule,
        MatButtonModule
    ],
    declarations: [
        SpeedDialFabComponent
    ],
    exports: [
        CommonModule,
        SpeedDialFabComponent,
        MatTooltipModule,
        MatButtonModule
    ]
})
export class FabButtonModule { }
