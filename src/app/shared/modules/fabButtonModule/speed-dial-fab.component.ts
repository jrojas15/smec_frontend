import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { speedDialFabAnimations } from './speed-dial-fab.animations';

@Component({
  selector: 'app-speed-dial-fab',
  templateUrl: './speed-dial-fab.component.html',
  styleUrls: ['./speed-dial-fab.component.scss'],
  animations: speedDialFabAnimations
})
export class SpeedDialFabComponent implements OnInit {

  fabButtons: IFabButton[];
  buttons: IFabButton[] = [];
  fabTogglerState = 'inactive';

  @Input() mode = 'flat' || 'nested';
  @Output() clicked = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.fabButtons = [
      {
        icon: 'timeline',
        tooltip: ''
      },
      {
        icon: 'view_headline',
        tooltip: ''
      },
      {
        icon: 'room',
        tooltip: ''
      },
      {
        icon: 'lightbulb_outline',
        tooltip: ''
      },
      {
        icon: 'history',
        tooltip: 'Descargar históricos en csv'
      }
    ];
  }

  showItems() {
    this.fabTogglerState = 'active';
    this.buttons = this.fabButtons;
  }

  hideItems() {
    this.fabTogglerState = 'inactive';
    this.buttons = [];
  }

  onToggleFab() {
    if (this.mode === 'nested') {
      this.buttons.length ? this.hideItems() : this.showItems();
    } else {
      this.clicked.emit();
    }
  }
}

export interface IFabButton {
  icon: string;
  tooltip: string;
}
