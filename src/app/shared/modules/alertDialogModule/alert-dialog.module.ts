import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertDialogComponent } from './alert-dialog.component';

import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';


@NgModule({
    imports: [
        CommonModule,
        MatDialogModule,
        MatButtonModule,
        FlexLayoutModule
    ],
    declarations: [AlertDialogComponent],
    entryComponents: [AlertDialogComponent]
})
export class AlertDialogModule { }
