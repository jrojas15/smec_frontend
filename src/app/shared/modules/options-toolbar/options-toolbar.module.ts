import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomToolbarComponent } from './custom-toolbar/custom-toolbar.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [CustomToolbarComponent],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule
  ],
  exports: [CommonModule, CustomToolbarComponent]
})
export class OptionsToolbarModule { }
