import { Component, OnInit, ViewChild, Input, EventEmitter, Output, ChangeDetectionStrategy } from '@angular/core';
import { MatMenuTrigger } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IAnalyzer, IFocus, ISelectionModel } from '@models';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-mat-selection',
  templateUrl: './mat-selection.component.html',
  styleUrls: ['./mat-selection.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
  // encapsulation: ViewEncapsulation.None

})
export class MatSelectionComponent implements OnInit {
  @ViewChild(MatMenuTrigger) menuTrigger: MatMenuTrigger;

  @Input() dataSource: IFocus[];

  @Output() emitData = new EventEmitter();

  analyzers = new Subject<IAnalyzer[]>();


  form: FormGroup;
  openedMenu = false;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      value1: ['', Validators.required],
      value2: [{ value: '', disabled: true }, Validators.required]
    });
  }

  get value1() { return this.form.get('value1'); }
  get value2() { return this.form.get('value2'); }


  onSubmit() {
    if (this.form.invalid) {
      return;
    }
    const data: ISelectionModel = {
      description: this.value1!.value.description + ' - ' + this.value2!.value.model,
      analyzer: this.value2!.value
    }
    this.emitData.emit(data);
    this.menuTrigger.closeMenu();
  }

  onSelectValue1(item: IFocus) {
    this.analyzers.next(item.analyzers);
    this.value2!.reset();
    this.value2!.setValue(null);
    this.value2!.enable();
  }

  openMenu() {
    this.openedMenu = true;
  }

  closeMenu() {
    this.openedMenu = false;
  }
}
