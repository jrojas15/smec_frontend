import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { LoadingService } from '@services/loading.service';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-loading-screen',
  templateUrl: './loading-screen.component.html',
  styleUrls: ['./loading-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class LoadingScreenComponent implements OnInit {
  loadingSubscription = this.loadingScreenService.loadingStatus.pipe(
    debounceTime(500));

  constructor(private loadingScreenService: LoadingService) { }

  ngOnInit() { }

}
