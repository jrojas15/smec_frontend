import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProfileService } from './profile.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.scss'],
  providers: [ProfileService]
})
export class ProfileViewComponent implements OnInit, OnDestroy {
  private ngUnsubscribe = new Subject();
  form: FormGroup;
  user: User;
  hide = true;
  _hide = true;
  __hide = true;
  show = false;
  error: boolean;
  Changepsswd = true;
  myContext = { $implicit: 'World', localSk: 'Svet' };
  message: string;
  progressBar = false;

  constructor(
    public dialogRef: MatDialogRef<ProfileViewComponent>,
    private _profileService: ProfileService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.getUser();
    this.form = this.fb.group({
      name: [`${this.data.username}`],
      currentPassword: ['', Validators.required],
      newPassword: [
        '',
        [Validators.required,
        Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]],
      confirmPassword: ['', Validators.required]

    });
  }
  /**
   * -- validator password partern --
   * min 8 character length
   * lowercase
   * uppercase
   * numbers
   * special char
   */

  get name() { return this.form.get('name'); }
  get currentPassword() { return this.form.get('currentPassword'); }
  get newPassword() { return this.form.get('newPassword'); }
  get confirmPassword() { return this.form.get('confirmPassword'); }

  getUser() {
    this._profileService.GetUser(this.data.username)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(res => {
        const [_user] = res;
        this.user = { userId: _user.userId, userName: _user.userName, roleNames: _user.roleNames };
      });
  }

  equalPasswords(): boolean {
    const matched: boolean = this.newPassword!.value === this.confirmPassword!.value;
    if (matched) {
      this.form.controls.confirmPassword.setErrors(null);
    } else {
      this.form.controls.confirmPassword.setErrors({
        notMatched: true
      });
    }
    return matched;
  }

  onSubmitPassword() {
    if (this.form.invalid) {
      return;
    }
    this.progressBar = true;
    if (this.currentPassword!.value !== null && this.newPassword!.value !== null) {
      this._profileService.updatePassword(this.name!.value, this.currentPassword!.value, this.newPassword!.value)
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(() => {
          this.error = false;
          this.show = true;
          setTimeout(() => {
            this.progressBar = false;
            this.dialogRef.close();
          }, 2000);

        }, (error) => {
          this.progressBar = false;
          if (`${error.code}` === '0') {
            this.onNoClick();
          }
          if (`${error.code}` === '400') {
            this.message = error.message;
            this.show = true;
            this.error = true;
          }
        });
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getCurrentPassswordErrorMessage() {
    return this.currentPassword!.hasError('required') ? 'Debes escribir una contraseña' :
      this.currentPassword!.hasError('currentPassword') ? 'Contraseña no válida' : '';
  }

  getNewPassswordErrorMessage() {
    return this.newPassword!.hasError('required') ? 'Debes escribir una contraseña' :
      this.newPassword!.hasError('newPassword') ? 'Contraseña no válida' :
        this.newPassword!.hasError('pattern') ? 'min 8 carácteres, minúsculas, mayúsculas, números y carácteres especiales' : '';
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

export interface User {
  userId: number;
  userName: string;
  roleNames: string[];
}
