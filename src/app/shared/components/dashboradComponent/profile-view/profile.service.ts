import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// @Injectable({
//   providedIn: 'root'
// })
@Injectable()
export class ProfileService {

  private http: HttpClient;

  constructor(@Inject(HttpClient) http: HttpClient) {
    this.http = http;
  }


  updatePassword(userName: string, currentPassword: string, newPassword: string) {
    return this.http.post('api/user/update/password/' + userName,
      { currentPassword: currentPassword, newPassword: newPassword });

  }

  GetUser(name: string) {
    return this.http.get<User[]>('api/user/' + name);
  }
}


export interface User {
  userId: number;
  userName: string;
  roleNames: string[];
}
