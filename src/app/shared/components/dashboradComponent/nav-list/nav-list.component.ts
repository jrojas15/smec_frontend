import { Component, OnInit, ChangeDetectionStrategy, Input, EventEmitter, Output } from '@angular/core';
import { CheckroleService } from '@services/checkrole.service';

@Component({
  selector: 'app-nav-list',
  templateUrl: './nav-list.component.html',
  styleUrls: ['./nav-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavListComponent implements OnInit {
  @Input() screenWidth: number;
  @Output() close = new EventEmitter();


  showSettingSubmenu = false;
  adminNavigationList: IAdminNavigation[];


  constructor(
    public _checkRole: CheckroleService
  ) { }

  ngOnInit() {
    this.adminNavigationList = [
      { path: 'gest-usr', active: 'active', title: 'Gestión de usuarios', hide: false }
    ];
  }

}

export interface IAdminNavigation {
  path: string;
  active: string;
  title: string;
  hide: boolean;
}
