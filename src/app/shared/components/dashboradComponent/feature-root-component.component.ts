import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';

import { ProfileViewComponent } from './profile-view/profile-view.component';
import { AuthService } from '@services/auth.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-feature-root-component',
  templateUrl: './feature-root-component.component.html',
  styleUrls: ['./feature-root-component.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class FeatureRootComponentComponent implements OnInit {
  username: string;

  screenWidthSource = new BehaviorSubject<number>(0);
  screenWidth = this.screenWidthSource.asObservable();

  constructor(
    public authService: AuthService,
    public dialog: MatDialog,
    private router: Router,
  ) { }

  ngOnInit() {
    this.handleSidenav();
    this.handleRouterAuth();
  }

  handleSidenav() {
    this.screenWidthSource.next(window.innerWidth);
    window.onresize = () => {
      this.screenWidthSource.next(window.innerWidth);
    };
  }

  handleRouterAuth() {
    if (this.authService.isLoggedIn()) {
      this.username = localStorage.getItem('currentUser')!;
      this.router.navigate(['features']);
    } else {
      this.router.navigate(['login']);
    }
  }


  logOut() {
    this.authService.logout();
  }

  viewProfile() {
    const dialogRef = this.dialog.open(ProfileViewComponent, {
      panelClass: 'progress-bar-dialog',
      width: '500px',
      data: { username: this.username }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
