import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-connection-error',
  templateUrl: './connection-error.component.html',
  styleUrls: ['./connection-error.component.scss'],
})
export class ConnectionErrorComponent implements OnInit {
  message: string;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.message = this.route.snapshot.paramMap.get('message')!;
  }

}
