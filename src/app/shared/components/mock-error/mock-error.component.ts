import { Component, OnInit, OnDestroy } from '@angular/core';
import { ErrorService } from '@services/error.service';
import { IError } from '@models';
import { Subject, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-mock-error',
  templateUrl: './mock-error.component.html',
  styleUrls: ['./mock-error.component.scss']
})
export class MockErrorComponent implements OnInit, OnDestroy {
  private ngUnsubscribe = new Subject();


  constructor(private _errorService: ErrorService) { }

  ngOnInit() { }

  notFound() {
    this._errorService.getNotFoundeError()
      .subscribe(() => console.log("getNotFoundeError ok")),
      (error: IError) => {
        console.log("getNotFoundeError", error)
        this._errorService.setError(error)
      };
  }

  badRequest() {
    this._errorService.getBadRequestError()
      .subscribe(() => console.log("getBadRequestError ok")),
      (error: IError) => {
        console.log("getBadRequestError", error)
        this._errorService.setError(error)
      };
  }

  notauthorized() {
    this._errorService.getUnauthorizedError()
      .subscribe(() => console.log("getUnauthorizedError ok")),
      (error: IError) => {
        console.log("getUnauthorizedError", error)
        this._errorService.setError(error)
      };
  }

  serverError() {
    this._errorService.getServerSideError()
      .subscribe(() => console.log("getServerSideError ok")),
      (error: IError) => {
        console.log("getServerSideError", error)
        this._errorService.setError(error)
      };
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
