import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MockErrorComponent } from './mock-error.component';

describe('MockErrorComponent', () => {
  let component: MockErrorComponent;
  let fixture: ComponentFixture<MockErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MockErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
