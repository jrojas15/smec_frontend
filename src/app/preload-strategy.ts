import { Route, PreloadingStrategy } from '@angular/router';
import { Observable, timer, of } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class AppPreloadingStrategy implements PreloadingStrategy {
    preloadedModules: string[] = [];

    preload(route: Route, load: () => Observable<any>): Observable<any> {
        // // console.log('route.data :', route.data);
        // // console.log('route.path :', route.path);

        if (route.data && route.data['preload']) {
            // add the route path to the preloaded module array
            this.preloadedModules.push(route.path!);
            // log the route path to the console
            // console.log('Preloaded: ' + route.path);
            return load();
        } else {
            return of(null);
        }
    }
}
