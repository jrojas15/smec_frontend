import '../polyfills';
/** Modules */
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';

import { CoreModule } from '@CoreModule';
import { LoginModule } from '@shared/modules/LoginModule/login.module';
import { MaterialModule } from '@material_modules';
import { ToastrModule } from 'ngx-toastr';

/** Components */
import { AppComponent } from './app.component';
import { ProfileViewComponent } from '@shared/components/dashboradComponent/profile-view/profile-view.component';
import { ConnectionErrorComponent } from '@shared/components/connectionErrorComponent/connection-error.component';
import { FeatureRootComponentComponent } from '@shared/components/dashboradComponent/feature-root-component.component';
import { NotFoundComponent } from '@shared/components/not-found/not-found.component';
import { NavListComponent } from '@shared/components/dashboradComponent/nav-list/nav-list.component';
import { LoadingScreenComponent } from '@shared/components/loading-screen/loading-screen.component';
import { MockErrorComponent } from './shared/components/mock-error/mock-error.component';

@NgModule({
  declarations: [
    AppComponent,
    ConnectionErrorComponent,
    FeatureRootComponentComponent,
    ProfileViewComponent,
    NotFoundComponent,
    NavListComponent,
    LoadingScreenComponent,
    MockErrorComponent
  ],
  imports: [
    CoreModule,
    AppRoutingModule,
    LoginModule,
    MaterialModule,
    ToastrModule.forRoot({
      closeButton: true,
      progressBar: true
    })
  ],
  exports: [],
  entryComponents: [ProfileViewComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
