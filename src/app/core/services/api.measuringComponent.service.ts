import { Observable } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IMeasuringComponent } from '@models';

@Injectable({
    providedIn: 'root'
})
export class MeasuringComponentService {
    private http: HttpClient;

    constructor(@Inject(HttpClient) http: HttpClient) {
        this.http = http;

    }

    measuringComponent_GetAll(): Observable<IMeasuringComponent[] | null> {
        return this.http.get<IMeasuringComponent[]>('api/MeasuringComponent');
    }

    measuringComponent_Create(item: IMeasuringComponent | null) {
        return this.http.post('api/MeasuringComponent', item);
    }

    measuringComponent_GetById(id: number): Observable<IMeasuringComponent[] | null> {
        return this.http.get<IMeasuringComponent[]>('api/MeasuringComponent/' + id);
    }

}
