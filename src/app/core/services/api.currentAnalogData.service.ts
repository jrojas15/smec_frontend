import { Observable } from 'rxjs';
import { Injectable, Inject, } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';
import { ICurrentAnalogData, ISerie, IChartAnalogData } from '@models';


@Injectable({
    providedIn: 'root'
})
export class CurrentAnalogDataService {
    private http: HttpClient;

    constructor(@Inject(HttpClient) http: HttpClient) {
        this.http = http;
    }

    sensor_GetCurrentAnalogData(id: number) {
        return this.http.get('api/Sensor/' + id + '/CurrentAnalogData');
    }

    sensor_GetHistoricalAnalogData(id: number, start_date: any, end_date: any): Observable<any[]> {
        start_date = moment(start_date).format('YYYY-MM-DD');
        end_date = moment(end_date).format('YYYY-MM-DD');

        return this.http.get<any[]>('api/Sensor/' + id + '/HistoricalAnalogData/' + start_date + '/' + end_date);
    }

    chart_GetHistoricalAnalogData(serie: ISerie[], start_date: Date, end_date: Date): Observable<IChartAnalogData[]> {
        const _start_date = moment(start_date).format('YYYY-MM-DD');
        const _end_date = moment(end_date).format('YYYY-MM-DD');
        return this.http.post<IChartAnalogData[]>('api/chartData/' + _start_date + '/' + _end_date, serie);
    }
}
