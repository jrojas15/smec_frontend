import { Injectable } from '@angular/core';
import { AuthorizationService as Authorization } from '@services/authorization.service';
import { Observable, of } from 'rxjs';

export interface NavigationList {
  path: string;
  active: string;
  title: string;
  hide: boolean | null;
  // group?: NavigationList[];
}

@Injectable({
  providedIn: 'root'
})
export class CheckroleService {
  private hide: boolean;
  private navigationListObs$: Observable<NavigationList[]>;

  constructor(private authorization: Authorization) {
    this.authorization.currentUserRoles
      .subscribe(roles => {
        const allowedRole = 'Administrador';
        if (Array.isArray(roles) && roles.includes(allowedRole)) {
          this.hide = false;
        }
        if (Array.isArray(roles) && !roles.includes(allowedRole)) {
          this.hide = true;
        }

        if (!Array.isArray(roles) && roles === allowedRole) {
          this.hide = false;
        }

        if (!Array.isArray(roles) && roles !== allowedRole) {
          this.hide = true;
        }
      });
  }


  chekForRoles(): boolean {
    return this.hide;
  }

  getNavigationList(): Observable<NavigationList[]> {
    return this.navigationListObs$ = of([
      { path: 'vis-rt', active: 'active', title: 'Visualización', hide: null },
      { path: 'graf-hist', active: 'active', title: 'Gráfico histórico', hide: !this.hide },
      { path: 'cal-func', active: 'active', title: 'Funciones de calibración', hide: this.hide },
      { path: 'periferic', active: 'active', title: 'Periféricos', hide: this.hide }
    ]);

  }


}

