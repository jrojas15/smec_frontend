import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, interval, throwError, of, Subject } from 'rxjs';
import { retryWhen, flatMap, take } from 'rxjs/operators';
import { Router } from '@angular/router';
import { IError } from '@models';

@Injectable({
    providedIn: 'root'
})
export class DataService {
    private data: any;
    private bool: boolean;

    private dataSource = new BehaviorSubject(this.data);
    private analyzerSource = new BehaviorSubject(this.data);
    private sensorSource = new BehaviorSubject(this.data);

    private widthSource = new BehaviorSubject(this.data);
    sidenavWidth = this.widthSource.asObservable();

    graphConfig: any;
    private graphConfigSubject = new BehaviorSubject(this.graphConfig);

    currentData = this.dataSource.asObservable();
    currentAnalyzer = this.analyzerSource.asObservable();
    currentSensor = this.sensorSource.asObservable();
    currentGraphConfig = this.graphConfigSubject.asObservable();

    private isRetrying: IError;
    private retrySource = new BehaviorSubject(this.isRetrying);

    retry = this.retrySource.asObservable();

    private stopLoadingSource = new BehaviorSubject(this.bool);
    stopLoadingSpinner = this.stopLoadingSource.asObservable();

    private theme: string;
    private themeSoruce = new BehaviorSubject(this.theme);
    currentTheme = this.themeSoruce.asObservable();


    constructor(private router: Router,
    ) { }

    setTheme(theme: string) {
        this.themeSoruce.next(theme);
    }

    setLoadingSpinner(data: boolean) {
        this.stopLoadingSource.next(data);
    }

    setRetry(data: IError) {
        this.retrySource.next(data);
    }
    // @ts-ignore
    setGraphConfig(data) {
        this.graphConfigSubject.next(data);
    }
    // @ts-ignore
    newData(data) {
        this.dataSource.next(data);
    }
    // @ts-ignore
    setSensor(data) {
        this.sensorSource.next(data);

    }
    // @ts-ignore
    setAnalyzer(data) {
        this.analyzerSource.next(data);
    }
    // @ts-ignore
    setSidenavWidth(data) {
        this.widthSource.next(data);
    }

}
