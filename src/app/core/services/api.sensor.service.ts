import { Observable } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ISensor } from '@models';

@Injectable({
    providedIn: 'root'
})
export class SensorService {
    private http: HttpClient;

    constructor(@Inject(HttpClient) http: HttpClient) {
        this.http = http;
    }

    sensor_GetAll(): Observable<ISensor[] | null> {
        return this.http.get<ISensor[]>('api/Sensor');
    }

    sensor_Create(item: ISensor | null): Observable<ISensor | null> {
        return this.http.post<ISensor>('api/Sensor', item);

    }

    sensor_GetById(id: number): Observable<ISensor | null> {
        return this.http.get<ISensor>('api/Sensor/' + id);
    }

    sensor_GetByAnalyzerId(id: number): Observable<ISensor[] | null> {
        return this.http.get<ISensor[]>('api/sensor/analyzer/' + id);
    }

    sensor_Delete(id: number) {
        return this.http.delete('api/Sensor/' + id);
    }

    sensor_Update(id: number, item: ISensor | null) {
        return this.http.patch('api/Sensor/' + id, item);
    }



}
