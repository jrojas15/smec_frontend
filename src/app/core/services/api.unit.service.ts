import { Observable } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IUnit } from '@models';
@Injectable({
    providedIn: 'root'
})
export class UnitService {
    private http: HttpClient;

    constructor(@Inject(HttpClient) http: HttpClient) {
        this.http = http;
    }

    unit_GetAll(): Observable<IUnit[] | null> {
        return this.http.get<IUnit[]>('api/Unit');
    }

    unit_Create(item: IUnit | null): Observable<IUnit | null> {
        return this.http.post<IUnit>('api/Unit', item)
    }

    unit_GetById(id: number): Observable<IUnit[] | null> {
        return this.http.get<IUnit[]>('api/Unit/' + id);
    }
}
