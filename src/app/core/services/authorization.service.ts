import { Injectable } from '@angular/core';
// @ts-ignore
import decode from 'jwt-decode';
import { AuthService } from './auth.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {
  data: any;
  private roles = new BehaviorSubject(this.data);
  currentUserRoles = this.roles.asObservable();

  constructor(private auth: AuthService) { }
  // @ts-ignore
  setRoles(data) {
    this.roles.next(data);
  }
  // @ts-ignore
  isAuthorized(allowedRoles: string[]): boolean {
    if (allowedRoles == null || allowedRoles.length === 0) {
      return true;
    }
    const token = this.auth.getAuth();

    const tokenPayload = decode(token!.token);

    if (!tokenPayload) {
      console.log('Invalid token');
      return false;
    }
    const tokenKey = 'http://schemas.microsoft.com/ws/2008/06/identity/claims/role';
    const tokenRoles = tokenPayload[tokenKey];
    this.setRoles(tokenRoles);
    if (Array.isArray(tokenRoles)) {
      for (let index = 0; index < tokenRoles.length; index++) {
        const element = tokenRoles[index];
        if (allowedRoles.includes(element)) {
          return true;
        }
      }
    } else {
      return allowedRoles.includes(tokenRoles);
    }

  }
}
