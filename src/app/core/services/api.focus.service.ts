import { Observable } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IFocus } from '@models';
import { tap } from 'rxjs/operators';
import { ErrorService } from './error.service';

@Injectable({
    providedIn: 'root'
})
export class FocusService {
    private http: HttpClient;


    constructor(
        @Inject(HttpClient) http: HttpClient,
        private _errorService: ErrorService
    ) {
        this.http = http;
    }

    focus_GetAll(): Observable<IFocus[]> {
        return this.http.get<IFocus[]>('api/Focus').pipe(
            tap(focus => {
                if (focus.length > 0) {
                  //  this._errorService.clearError();
                }
            }),
            /*  catchError((error: IError) => {
                  this._errorService.setError(error);
                  return throwError(error);
              })*/
        );
    }

    focus_GetById(id: number): Observable<IFocus[]> {
        return this.http.get<IFocus[]>('api/Focus/' + id);
    }

    focus_Delete(id: number) {
        return this.http.delete('api/Focus/' + id);
    }

    focus_Update(id: number, item: IFocus) {
        return this.http.patch('api/Focus/' + id, item);
    }

    focus_GetCurrentStatus(id: number) {
        return this.http.get('api/Focus/' + id + '/CurrentStatus');
    }

    focus_Create(item: IFocus): Observable<IFocus> {
        return this.http.post<IFocus>('api/Focus/Create', item);
    }

}
