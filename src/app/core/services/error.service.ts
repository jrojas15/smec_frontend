import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IError } from '@models';
import { HttpClient } from '@angular/common/http';
import { shareReplay, take, skipUntil, skipWhile } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {
  private http: HttpClient;

  private api = 'api/errorMock/'

  private error: IError;
  private errorSource$ = new BehaviorSubject<IError>(this.error);

  errorObs$ = this.errorSource$.asObservable().pipe(shareReplay()/*,skipWhile(err => err !== null)*/);

  constructor(@Inject(HttpClient) http: HttpClient) {
    this.http = http;
  }

  getNotFoundeError() {
    return this.http.get(this.api + 'notFound');
  }

  getBadRequestError() {
    return this.http.get(this.api + 'badRequest');
  }

  getUnauthorizedError() {
    return this.http.get(this.api + 'unauthorized');
  }

  getServerSideError() {
    return this.http.get(this.api + 'serverError');
  }

  setError(error: IError) {
    if (error) {
      this.errorSource$.next(error);
    }
  }
  clearError() {
    this.errorSource$.next(this.error);
  }
}
