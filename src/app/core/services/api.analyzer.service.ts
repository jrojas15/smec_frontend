import { Observable } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IAnalyzer } from '@models';

@Injectable({
    providedIn: 'root'
})
export class AnalyzerService {
    private http: HttpClient;

    constructor(@Inject(HttpClient) http: HttpClient) {
        this.http = http;
    }

    analyzer_GetAll(): Observable<IAnalyzer[] | null> {
        return this.http.get<IAnalyzer[]>('api/Analyzer');
    }

    analyzer_Create(item: IAnalyzer | null): Observable<IAnalyzer | null> {
        return this.http.post<IAnalyzer>('api/Analyzer', item);
    }

    analyzer_GetById(id: number): Observable<IAnalyzer[] | null> {
        return this.http.get<IAnalyzer[]>('api/Analyzer/' + id);
    }

    analyzer_Delete(id: number) {
        return this.http.delete('api/Analyzer/' + id);
    }


    analyzer_Update(id: number, item: IAnalyzer | null) {
        return this.http.patch('api/Analyzer/' + id, item);
    }

    analyzer_GetCurrentStatus(id: number) {
        return this.http.get('api/Analyzer/' + id + '/CurrentStatus');
    }
}
