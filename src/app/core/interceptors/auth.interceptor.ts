import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHandler, HttpEvent, HttpInterceptor, HttpRequest, HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { AuthService } from '@services/auth.service';

import { ToastrService } from 'ngx-toastr';
import { ErrorService } from '@services/error.service';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    /** Lista de apis que se excluyen de  HttpInterceptor */
    excludedUrls = [
        '/sensor/amchart',
        '/token/auth',
        '/user/role',
        '/chartData'
    ]

    constructor(
        private injector: Injector,
        private router: Router,
        private toastr: ToastrService,
        private _errorService: ErrorService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const auth = this.injector.get(AuthService);
        const token = (auth.isLoggedIn()) ? auth.getAuth()!.token : null;
        let loadUrl = true;
        if (token) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${token}`
                }
            });
        } return next.handle(request)
            .pipe(
                tap(evt => {
                    if (evt instanceof HttpResponse && request.method !== "GET") {
                        for (const excludedUrl of this.excludedUrls) {
                            if (new RegExp(excludedUrl).test(request.url)) {
                                loadUrl = false;
                                break;
                            }
                        }

                        if (loadUrl && evt.ok) {
                            this.toastr.success('Acción realizada con éxito.', '', {
                                progressBar: true
                            });
                        }
                    }
                }),
                catchError((error: HttpErrorResponse) => {
                    let errorMessage;
                    if (error.error instanceof ErrorEvent) {
                        // client-side error
                        console.log('client-side error', error.error);
                        errorMessage = { message: `client-side Error: ${error.error.message}` };
                    } else {
                        // server-side error
                        const messages = {
                          0: () => {
                            this.router.navigate(['error',{ code: error.status, message: `Se ha perdido la conexión con el servidor` }]);
                          },
                          500: () => {
                            this.toastr.error('Error en el servidor', '');
                          },
                          400:()=> {
                            this.toastr.error(`${error.error.message}` + '.', '');
                          },
                          401:()=> {
                            this.toastr.warning('Acción no autorizada', '');
                          },
                          404:()=> {
                            this._errorService.setError({ code: error.status, message: `${error.error.message}` });
                          },
                          'default':()=> {
                            errorMessage = { code: error.status, message: `${error.error.message}` };
                          }
                        }

                        if(messages[error.status]) {
                         messages[error.status];
                        } else {
                          messages['default'];
                        }
                    }
                    return throwError(errorMessage);
                })
            )
    }
}

