import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let baseUrl = document.getElementsByTagName('base')[0].href;
        if (baseUrl === 'http://localhost:4200/') {
            baseUrl = 'https://localhost:5001/';
            const apiReq = req.clone({ url: `${baseUrl}${req.url}` });
            return next.handle(apiReq);
        } else {
            const re = /.$/gi;
            const newUrl = baseUrl.replace(re, ':4443/');
            const apiReq = req.clone({ url: `${newUrl}${req.url}` });
            return next.handle(apiReq);
        }
    }
}
