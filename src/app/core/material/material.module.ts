import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSliderModule } from '@angular/material/slider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatNativeDateModule } from '@angular/material';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MAT_DATE_LOCALE } from '@angular/material';
import { MatTabsModule } from '@angular/material/tabs';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatTreeModule } from '@angular/material/tree';



@NgModule({
    imports: [
        CommonModule,
        MatTableModule,
        MatProgressBarModule,
        MatPaginatorModule,
        MatSortModule,
        MatCardModule,
        MatStepperModule,
        MatToolbarModule,
        MatButtonToggleModule,
        MatFormFieldModule,
        MatSidenavModule,
        MatCheckboxModule,
        MatRadioModule,
        MatInputModule,
        MatListModule,
        MatIconModule,
        MatSlideToggleModule,
        MatSelectModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatSliderModule,
        MatExpansionModule,
        MatMenuModule,
        MatSnackBarModule,
        MatDialogModule,
        MatTooltipModule,
        FlexLayoutModule,
        MatTabsModule,
        DragDropModule,
        MatTreeModule
    ],
    exports: [
        MatTableModule,
        MatProgressBarModule,
        MatPaginatorModule,
        MatSortModule,
        MatCardModule,
        MatStepperModule,
        MatToolbarModule,
        MatButtonToggleModule,
        MatFormFieldModule,
        MatSidenavModule,
        MatCheckboxModule,
        MatRadioModule,
        MatInputModule,
        MatListModule,
        MatIconModule,
        MatSlideToggleModule,
        MatSelectModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatSliderModule,
        MatExpansionModule,
        MatMenuModule,
        MatSnackBarModule,
        MatDialogModule,
        MatTooltipModule,
        FlexLayoutModule,
        MatTabsModule,
        DragDropModule,
        MatTreeModule
    ],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-419' },
        MatDatepickerModule
    ]

})
export class MaterialModule {

}
