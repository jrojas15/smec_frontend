import { Injectable } from '@angular/core';
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    ActivatedRoute,
    Router,
    CanActivateChild
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '@services/auth.service';
import { AuthorizationService } from '@services/authorization.service';

@Injectable({
    providedIn: 'root'
  })
export class RoleGuardService implements CanActivate, CanActivateChild {
    constructor(public auth: AuthService, private authorizationService: AuthorizationService, public router: Router) { }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        const allowedRoles = next.data.allowedRoles;
        const isAuthorized = this.authorizationService.isAuthorized(allowedRoles);
        if (!isAuthorized) {
            this.router.navigate(['features/' + '']);
        }
        return isAuthorized;
    }

    canActivateChild(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        const allowedRoles = next.data.allowedRoles;
        const isAuthorized = this.authorizationService.isAuthorized(allowedRoles);
        if (!isAuthorized) {
            this.router.navigate(['features/' + '']);
        }
        return isAuthorized;
    }
}

export interface Role {
    admin: string;
    user: string;
}
