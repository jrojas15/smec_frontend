/* tslint:disable */
import * as moment from 'moment';

export interface TokenResponse {
    token: string;
    expiration: number;
}

export interface IFocus {
    focusId: number;
    name: string;
    description: string;
    inService: number;
    analyzers: IAnalyzer[];
    series: ISerie[];
}

export interface IAnalyzer {
    analyzerId: number;
    focusId: number;
    manufacturer: string;
    model: string;
    serialNumber: string;
    sensors: ISensor[];
    focus: IFocus;
    IsSelected?: boolean;
}

export interface ICalibrationFunction {
    calibrationFunctionId?: number;
    sensorId: number;
    date: moment.Moment;
    slope: number;
    intercept: number;
    sensor: ISensor;
    isEditable?: boolean;
    name: string
}

export interface ISensor {
    sensorId: number;
    analyzerId: number;
    measuringComponentId: number;
    unitId: number;
    calibrationFunctions: ICalibrationFunction[];
    analyzer: IAnalyzer;
    measuringComponent: IMeasuringComponent;
    unit: IUnit;
    periferic: IPeriferic;
    corrections: ICorrection[];
    isSelected: boolean;
}

export interface IPeriferic {
    perifericId: number;
    sensorId: number;
    name?: string;
    perifericFormula?: IFormula;
    isEditable?: boolean;
}

export interface IPerifericFormula {
    perifericId: number;
    formulaId: number;
    formula?: IFormula;
    periferic?: IPeriferic;
}

export interface ICurrentAnalogData {
    value: number;
    statusCode: number;
    samples: number;
}

export interface IMeasuringComponent {
    measuringComponentId: number;
    name: string;
}

export interface IUnit {
    unitId: number;
    name: string;
}

export interface ISerie {
    serieId?: number;
    sensorId: number;
    chartId: number;
    name: string;
    sensor?: ISensor;
    date?: string;
}

export interface IChart {
    chartId: number;
    description: string;
    series?: ISerie[];
}

export interface IChartForm {
    chartId: number;
    initialDate: Date;
    finalDate: Date;
}

export interface IFormula {
    formulaId: number;
    name: string;
    value: string;
    perifericFormula: IPerifericFormula;
}

export interface ICorrection {
    sensorId: number;
    perifericId: number;
    perifericFormulas?: IPerifericFormula[];
}

export interface ICorrectionDTO {
    sensorId: number;
    name: string;
    perifericId: number;
    formula: IFormula;
}

export interface IError {
    code: number;
    message: string;
}

export interface IDataSource {
    isDataReady: boolean,
    isDataNull: boolean,
    dataProvider: any[],
    series: ISerie[],
    init_date: Date,
    end_date: Date,
    focusDescription: string,
    message: string
}

export interface IAlertDialog {
    message: string;
    dismiss: string;
    accept: string;
}

export class DisplayMessages {
    title: string;
    message: string;
    isDataEmpty: boolean;
    image: DefaultImages;
}

export enum DefaultImages {
    empty_data = 'assets/emptyData.svg',
    choose_items = 'assets/choose.svg',
    empty_chart = 'assets/emptyChart.svg',
    warn = 'assets/warn.svg'
}

export interface IMatSelection {
    desc: string;
    analyzer: IAnalyzer;
}

export interface IChartAnalogData{
    [key: string]: number | string;
}

export interface ISelectionModel {
    description: string;
    analyzer: IAnalyzer;
}